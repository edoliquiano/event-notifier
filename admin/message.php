<?php
include("../configurations/adminconfig.php");

$cMap = array(
    "message" => "message.con.php"
);

$cmapKey = isset($_GET['inpage'])?$_GET['inpage']:'default';

if(isset($_GET['inpage']) && !empty($_GET['inpage']) && array_key_exists($_GET['inpage'],$cMap)){
    include(SYSCONFIG_MODULE_PATH."message/".$cMap[$cmapKey]);
}else {
    include(SYSCONFIG_MODULE_PATH."admin/admin.con.php");
}