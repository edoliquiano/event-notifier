<?php
include("../configurations/adminconfig.php");

$cMap = array(
    "campus" => "campus.con.php"
    ,"department" => "department.con.php"
    ,"course" => "course.con.php"
    ,"site" => "site.con.php"
    ,"slider" => "slider.con.php"
    ,"account" => "account.con.php"
);

$cmapKey = isset($_GET['inpage'])?$_GET['inpage']:'default';

if(isset($_GET['inpage']) && !empty($_GET['inpage']) && array_key_exists($_GET['inpage'],$cMap)){
    include(SYSCONFIG_MODULE_PATH."setup/".$cMap[$cmapKey]);
}else {
    include(SYSCONFIG_MODULE_PATH."admin/admin.con.php");
}