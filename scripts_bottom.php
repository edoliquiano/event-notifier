<?php

?>
<!-- jQuery -->
<script src="../components/vendors/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap -->
<script src="../components/vendors/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- FastClick -->
<script src="../components/vendors/fastclick/lib/fastclick.js"></script>

<!-- NProgress -->
<script src="../components/vendors/nprogress/nprogress.js"></script>

<!-- Chart.js -->
<script src="../components/vendors/Chart.js/dist/Chart.min.js"></script>

<!-- gauge.js -->
<script src="../components/vendors/gauge.js/dist/gauge.min.js"></script>

<!-- Switchery -->
<script src="../components/vendors/switchery/dist/switchery.min.js"></script>

<!-- bootstrap-progressbar -->
<script src="../components/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>

<!-- iCheck -->
<script src="../components/vendors/iCheck/icheck.min.js"></script>

<!-- Skycons -->
<script src="../components/vendors/skycons/skycons.js"></script>

<!-- Flot -->
<script src="../components/vendors/Flot/jquery.flot.js"></script>
<script src="../components/vendors/Flot/jquery.flot.pie.js"></script>
<script src="../components/vendors/Flot/jquery.flot.time.js"></script>
<script src="../components/vendors/Flot/jquery.flot.stack.js"></script>
<script src="../components/vendors/Flot/jquery.flot.resize.js"></script>

<!-- Flot plugins -->
<script src="../components/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
<script src="../components/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
<script src="../components/vendors/flot.curvedlines/curvedLines.js"></script>

<!-- DateJS -->
<script src="../components/vendors/DateJS/build/date.js"></script>

<!-- JQVMap -->
<script src="../components/vendors/jqvmap/dist/jquery.vmap.js"></script>
<script src="../components/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
<script src="../components/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>

<!-- jQuery Tags Input -->
<script src="../components/vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>

<!-- Select2 -->
<script src="../components/vendors/select2/dist/js/select2.full.js"></script>

<script src="../components/vendors/moment/min/moment.min.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="../components/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap-datetimepicker -->
<script src="../components/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<!-- FullCalendar -->
<script src="../components/vendors/fullcalendar/dist/fullcalendar.min.js"></script>

<!-- Custom Theme Scripts -->
<script src="../components/build/js/custom.js"></script>

<script type="text/javascript">
    $("#participantInput").select2({
        placeholder: "Participants",
        multiple: true,
        data:[<?=$participantList?>],
//        maximumSelectionLength: 2,
        width: '100%',
        allowClear: true
    });

    $('#startDatePicker').datetimepicker({
        format: 'YYYY-MM-DD',
        useCurrent: true,
        defaultDate: "2019-01-06"
    });

    $('#endDatePicker').datetimepicker({
        format: 'YYYY-MM-DD',
        useCurrent: true,
        defaultDate: "2019-01-06"
    });

    $('#startTimePicker').datetimepicker({
        format: 'hh:mm A',
        useCurrent: false
    });

    $('#endTimePicker').datetimepicker({
        format: 'hh:mm A',
        useCurrent: false
    });

    function  init_calendar() {

        if( typeof ($.fn.fullCalendar) === 'undefined'){ return; }
        console.log('init_calendar');

        var date = new Date(),
            d = date.getDate(),
            m = date.getMonth(),
            y = date.getFullYear(),
            started,
            categoryClass;

        var calendar = $('#calendar').fullCalendar({
            <?=$_SESSION[SYSTEM_SESSION]['user_id'] > 0 ? "customButtons: {
                myCustomButton: {
                    text: 'Add Event',
                    classes: 'btn btn-success',
                    click: function() {
                        $('#event_title').val('');
                        $('#event_desc').val('');
                        $('#CalenderModalNew').modal('show');
                    }
                }
            }," : ""?>
            header: {
                left: 'myCustomButton prevYear,prev,next,nextYear today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay,listMonth'
            },
            selectable: <?=$_SESSION[SYSTEM_SESSION]['user_id'] > 0 ? 'true' : 'false'?>,
            selectHelper: <?=$_SESSION[SYSTEM_SESSION]['user_id'] > 0 ? 'true' : 'false'?>,
            select: function(start, end, allDay) {
                $('#fc_create').click();

                started = start;
                ended = end;

                var startDate = new Date(start);
                var endDate = new Date(end);
                endDate.setDate(endDate.getDate() - 1);

                $('#startDatePicker').data("DateTimePicker").date(startDate);
                $('#endDatePicker').data("DateTimePicker").date(endDate);
                $('#startTimePicker').data("DateTimePicker").date(null);
                $('#endTimePicker').data("DateTimePicker").date(null);
                $('#event_title').val('');
                $('#event_desc').val('');
                $('#event_id').val('');

                loadParticipants();

                $(".antosubmit").on("click", function() {
                    var title = $("#title").val();
                    if (end) {
                        ended = end;
                    }

                    categoryClass = $("#event_type").val();

                    if (title) {
                        calendar.fullCalendar('renderEvent', {
                                title: title,
                                start: started,
                                end: end,
                                allDay: allDay
                            },
                            true // make the event "stick"
                        );
                    }


                    $('#event_title').val('');

                    calendar.fullCalendar('unselect');

                    $('.antoclose').click();

                    return false;
                });
            },
            eventClick: function(calEvent, jsEvent, view) {

                <?=$_SESSION[SYSTEM_SESSION]['user_id'] > 0 ? "$('#fc_create').click();" : "$('#fc_edit').click();"?>
                <?=$_SESSION[SYSTEM_SESSION]['user_id'] > 0 ? "$('#event_title').val(calEvent.title);" : "$('#event_titleDiv').val(calEvent.title);"?>
                <?=$_SESSION[SYSTEM_SESSION]['user_id'] > 0 ? "$('#event_desc').val(calEvent.desc);" : "$('#event_descDiv').val(calEvent.desc);"?>
                <?=$_SESSION[SYSTEM_SESSION]['user_id'] > 0 ? "$('#event_id').val(calEvent.event_id);" : "$('#event_id_view').val(calEvent.event_id);"?>

                categoryClass = $("#event_type").val();

                loadParticipants();

                var startTempClickDate = moment(calEvent.start).format("YYYY-MM-DD");
                var endTempClickDate = moment(calEvent.end).subtract(1, "days").format("YYYY-MM-DD");

                var startTempClickTime = moment(calEvent.start).format("HH:mm A");
                var startTempClickTime2 = moment(calEvent.start).format("hh:mm A");
                var endTempClickTime = moment(calEvent.end).subtract(1, "days").format("HH:mm A");
                var endTempClickTime2 = moment(calEvent.end).subtract(1, "days").format("hh:mm A");

                <?=$_SESSION[SYSTEM_SESSION]['user_id'] > 0 ? "$('#startDatePicker').data('DateTimePicker').date(startTempClickDate);" : "$('#startDatePickerDiv').val(startTempClickDate);"?>
                <?=$_SESSION[SYSTEM_SESSION]['user_id'] > 0 ? "$('#endDatePicker').data('DateTimePicker').date(endTempClickDate);" : "$('#endDatePickerDiv').val(endTempClickDate);"?>
                <?=$_SESSION[SYSTEM_SESSION]['user_id'] > 0 ? "$('#startTimePicker').data('DateTimePicker').date(startTempClickTime);" : "$('#startTimePickerDiv').val(startTempClickTime2);"?>
                <?=$_SESSION[SYSTEM_SESSION]['user_id'] > 0 ? "$('#endTimePicker').data('DateTimePicker').date(endTempClickTime);" : "$('#endTimePickerDiv').val(endTempClickTime2);"?>

//                $(".antosubmit2").on("click", function() {
//                    calEvent.title = $("#event_title").val();
//
//                    calendar.fullCalendar('updateEvent', calEvent);
//                    $('.antoclose2').click();
//                });

                calendar.fullCalendar('unselect');
            },
            eventResize: function(calEvent, jsEvent, view){

                <?=$_SESSION[SYSTEM_SESSION]['user_id'] > 0 ? "$('#fc_create').click();" : ""?>

                $('#event_title').val(calEvent.title);
                $('#event_desc').val(calEvent.desc);
                $('#event_id').val(calEvent.event_id);

                categoryClass = $("#event_type").val();

                loadParticipants();

                var startTempClickDate = moment(calEvent.start).format("YYYY-MM-DD");
                var endTempClickDate = moment(calEvent.end).subtract(1, "days").format("YYYY-MM-DD");

                var startTempClickTime = moment(calEvent.start).format("HH:mm A");
                var endTempClickTime = moment(calEvent.end).subtract(1, "days").format("HH:mm A");

                $('#startDatePicker').data("DateTimePicker").date(startTempClickDate);
                $('#endDatePicker').data("DateTimePicker").date(endTempClickDate);
                $('#startTimePicker').data("DateTimePicker").date(startTempClickTime);
                $('#endTimePicker').data("DateTimePicker").date(endTempClickTime);

            },
            eventDrop: function(calEvent, jsEvent, ui, view){

                <?=$_SESSION[SYSTEM_SESSION]['user_id'] > 0 ? "$('#fc_create').click();" : ""?>

                $('#event_title').val(calEvent.title);
                $('#event_desc').val(calEvent.desc);
                $('#event_id').val(calEvent.event_id);
//
//                categoryClass = $("#event_type").val();

                loadParticipants();

                var startTempClickDate = moment(calEvent.start).format("YYYY-MM-DD");
                var endTempClickDate = moment(calEvent.end).subtract(1, "days").format("YYYY-MM-DD");

                var startTempClickTime = moment(calEvent.start).format("HH:mm A");
                var endTempClickTime = moment(calEvent.end).subtract(1, "days").format("HH:mm A");

                $('#startDatePicker').data("DateTimePicker").date(startTempClickDate);
                $('#endDatePicker').data("DateTimePicker").date(endTempClickDate);
                $('#startTimePicker').data("DateTimePicker").date(startTempClickTime);
                $('#endTimePicker').data("DateTimePicker").date(endTempClickTime);

            },
            editable: <?=$_SESSION[SYSTEM_SESSION]['user_id'] > 0 ? 'true' : 'false'?>,
            <?=$eventTableList?>
//            events: [{
//                title: 'All Day Event',
//                allDay: true,
//                start: new Date('2019-01-23')
//            }, {
//                title: 'Long asd',
//                allDay: false,
//                start: new Date("2019-01-06 12:00:00"),
//                end: new Date("2019-01-06 20:00:00")
//            }, {
//                title: 'Meeting',
//                start: new Date(y, m, d, 10, 30),
//                allDay: false
//            }, {
//                title: 'Lunch',
//                start: new Date(y, m, d + 14, 12, 0),
//                end: new Date(y, m, d, 14, 0),
//                allDay: false
//            }, {
//                title: 'Birthday Party',
//                start: new Date(y, m, d + 1, 19, 0),
//                end: new Date(y, m, d + 1, 22, 30),
//                allDay: false
//            }, {
//                title: 'Click for Google',
//                start: new Date(y, m, 28),
//                end: new Date(y, m, 29),
//                url: 'http://google.com/'
//            }]
        });

    };
</script>
<?php

?>