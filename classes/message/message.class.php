<?php

class clsMessageSetup{

    var $conn;
    var $fieldMap;
    var $Data;

    /**ss
     * Class Constructor
     *
     * @param object $dbconn_
     * @return clsMessageSetup object
     */
    function clsMessageSetup($dbconn_ = null){
        $this->conn =& $dbconn_;
        $this->fieldMap = array(
            "dm_number" => "dm_number",
            "dm_content" => "dm_content",
            "student_id" => "student_id"
        );
    }

    /**
     * Get the records from the database
     *
     * @param string $id_
     * @return array
     */
    function dbFetch($id_ = ""){
        $sql = "SELECT * FROM db_event_notif.db_message WHERE dm_id = ?";
        $rsResult = $this->conn->Execute($sql,array($id_));
        if(!$rsResult->EOF){
            return $rsResult->fields;
        }
    }
    /**
     * Populate array parameters to Data Variable
     *
     * @param array $pData_
     * @return bool
     */
    function doPopulateData($pData_ = array()){
        if(count($pData_)>0){
            foreach ($this->fieldMap as $key => $value) {
                $this->Data[$key] = $pData_[$value];
            }
            return true;
        }
        return false;
    }

    /**
     * Validation function
     *
     * @param array $pData_
     * @return bool
     */
    function doValidateData($pData_ = array()){
        $isValid = true;

//		$isValid = false;

        return $isValid;
    }

    /**
     * Save New
     *
     */
    function doSaveAdd(){
        $flds = array();
        foreach ($this->Data as $keyData => $valData) {
            $flds[] = "$keyData='$valData'";
        }
        $fields = implode(", ",$flds);

        $sql = "INSERT INTO db_event_notif.db_message SET $fields";
        $this->conn->Execute($sql);

        $this->sendMessage($this->Data['dm_content'], $this->Data['dm_number']);

        $_SESSION[SYSTEM_SESSION]['eMsg']="Successfully Added.";
    }

    /**
     * Save Update
     *
     */
    function doSaveEdit(){
        $id = $_GET['edit'];

        $flds = array();
        foreach ($this->Data as $keyData => $valData) {
            $flds[] = "$keyData='$valData'";
        }
        $fields = implode(", ",$flds);

        $sql = "UPDATE db_event_notif.db_message SET $fields WHERE dm_id=$id";
        $this->conn->Execute($sql);
        $_SESSION[SYSTEM_SESSION]['eMsg']="Successfully Updated.";
    }

    /**
     * Delete Record
     *
     * @param string $id_
     */
    function doDelete($id_ = ""){
        $sql = "DELETE FROM db_event_notif.db_message WHERE dm_id=?";
        $this->conn->Execute($sql,array($id_));
        $_SESSION[SYSTEM_SESSION]['eMsg']="Successfully Deleted.";
    }

    /**
     * Get all the Table Listings
     *
     * @return array
     */
    function getTableList(){
        // Process the query string and exclude querystring named "p"
        if (!empty($_SERVER['QUERY_STRING'])) {
            $qrystr = explode("&",$_SERVER['QUERY_STRING']);
            foreach ($qrystr as $value) {
                $qstr = explode("=",$value);
                if ($qstr[0]!="p") {
                    $arrQryStr[] = implode("=",$qstr);
                }
            }
            $aQryStr = $arrQryStr;
            $aQryStr[] = "p=@@";
            $queryStr = implode("&",$aQryStr);
        }

        //bby: search module
        $qry = array();
        if (isset($_REQUEST['search_field'])) {

            // lets check if the search field has a value
            if (strlen($_REQUEST['search_field'])>0) {
                // lets assign the request value in a variable
                $search_field = MainBlock::BlockSQLInjection($_REQUEST['search_field']);

                // create a custom criteria in an array
                $qry[] = "(dm.dm_number like '%$search_field%'
                            OR dm.dm_content like '%$search_field%'
                            OR if(dm.is_active = 1, concat('Active'), concat('Not Active')) like '%$search_field%'
                            )";

            }
        }

        // put all query array into one criteria string
        $criteria = (count($qry)>0)?" where ".implode(" and ",$qry):"";

        $viewLink = "<a href=\"?inpage=course&view=',dm.dm_id,'\" class=\"btn btn-sm btn-success\">View</a>";
        $editLink = "<a href=\"?inpage=course&edit=',dm.dm_id,'\" class=\"btn btn-sm btn-success\">EDIT</a>";
        $delLink = "<a href=\"?inpage=course&delete=',dm.dm_id,'\" onclick=\"return confirm(\'Are you sure, you want to delete?\');\" class=\"btn btn-flat btn-xs bg-orange\"><span class=\"glyphicon glyphicon-trash\"></span> DELETE</a>";

        // Sort field mapping
        $arrSortBy = array(
            "dm_number"=>"dm.dm_number"
        ,"dm_content"=>"dm.dm_content"
        ,"department_shortname"=>"dd.department_shortname"
        ,"campus_shortname"=>"dc.campus_shortname"
        ,"status_ref"=>"if(dm.is_active = 1, concat('Active'), concat('Not Active'))"
        );

        if(isset($_GET['sortby'])){
            $strOrderBy = " order by ".$arrSortBy[$_GET['sortby']]." ".$_GET['sortof'];
        }

        $sql = "SELECT *, concat('$viewLink') as viewdata
                FROM db_event_notif.db_message dm
                LEFT JOIN db_event_notif.db_student ds on dm.student_id = ds.student_id
                $criteria
                $strOrderBy
                ";

        $sqlcount = "SELECT count(*) as mycount FROM db_event_notif.db_message order by dm_id $criteria";

        $arrFields = array(
            "dm_number"=>"Number",
            "dm_content"=>"Content",
            "viewdata"=>"&nbsp;"
        );

        $arrAttribs = array(
            "dm_number"=>"style=\"white-space: nowrap;width: 1px; vertical-align: top;\""
            ,"dm_content"=>"style=\"vertical-align: top;\""
            ,"viewdata"=>" style=\"white-space: nowrap;width: 1px\""
        );

        $tblDisplayList = new clsTableList($this->conn);
        $tblDisplayList->arrFields = $arrFields;
        $tblDisplayList->paginator->linkPage = "?$queryStr";
        $tblDisplayList->sqlAll = $sql;
        $tblDisplayList->sqlCount = $sqlcount;

        return $tblDisplayList->getTableList($arrAttribs);
    }

    function updateStudentsSms($message, $student_id = array()){
        $message = addslashes($message);
        if(count($student_id) > 0){
            foreach($student_id as $key=>$value){
                $sqlGetStudentInfo = "SELECT *
                                        FROM dp_event_notif.db_student
                                        WHERE student_id = '{$value}'
                                        ";
                $rsGSI = $this->conn->Execute($sqlGetStudentInfo);
                if(!$rsGSI->EOF){
                    $flds[] = "dm_number = '{$rsGSI->fields['student_mobile']}'";
                    $flds[] = "dm_content = '{$message}'";
                    $flds[] = "dm_addwhen = NOW()";
                    $flds[] = "dm_addwho = '{$_SESSION[SYSTEM_SESSION]['user_name']}'";
                    $flds[] = "student_id = '{$value}'";
                    $fields = implode(', ', $flds);
                    $sqlInsert = "INSERT INTO db_event_notif.db_message SET $fields";
                    $rsInsert = $this->conn->Execute($sqlInsert);
                    if($rsInsert){
                        $this->sendMessage($message, $rsGSI->fields['student_mobile']);
                    }
                    unset($flds);
                }
            }
        }
    }

    function sendMessage($message, $number){
        $messageTemp = addslashes($message);
        $timeNow = date("Ymd_His");
        $numberTemp = str_split($number);
        if(count($numberTemp)<11){
            $numberTemp[0] = "+63";
        }elseif($number[0] == 0){
            $numberTemp[0] = "+63";
        }
        $numberTemp = implode('',$numberTemp);
        $myfile = fopen("C:/cygwin64/var/spool/sms/outgoing/{$timeNow}", "w") or die("Unable to open file!");
        $txt = "To: {$numberTemp}

{$messageTemp}";
        fwrite($myfile, $txt);
        fclose($myfile);
    }

}


?>
