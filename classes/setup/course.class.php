<?php

class clsCourseSetup{

    var $conn;
    var $fieldMap;
    var $Data;

    /**ss
     * Class Constructor
     *
     * @param object $dbconn_
     * @return clsCourseSetup object
     */
    function clsCourseSetup($dbconn_ = null){
        $this->conn =& $dbconn_;
        $this->fieldMap = array(
            "course_name" => "course_name",
            "course_shortname" => "course_shortname",
            "department_id" => "department_id"
        );
    }

    /**
     * Get the records from the database
     *
     * @param string $id_
     * @return array
     */
    function dbFetch($id_ = ""){
        $sql = "SELECT * FROM db_event_notif.db_course WHERE course_id = ?";
        $rsResult = $this->conn->Execute($sql,array($id_));
        if(!$rsResult->EOF){
            return $rsResult->fields;
        }
    }
    /**
     * Populate array parameters to Data Variable
     *
     * @param array $pData_
     * @return bool
     */
    function doPopulateData($pData_ = array()){
        if(count($pData_)>0){
            foreach ($this->fieldMap as $key => $value) {
                $this->Data[$key] = $pData_[$value];
            }
            return true;
        }
        return false;
    }

    /**
     * Validation function
     *
     * @param array $pData_
     * @return bool
     */
    function doValidateData($pData_ = array()){
        $isValid = true;

//		$isValid = false;

        return $isValid;
    }

    /**
     * Save New
     *
     */
    function doSaveAdd(){
        $flds = array();
        foreach ($this->Data as $keyData => $valData) {
            $flds[] = "$keyData='$valData'";
        }
        $fields = implode(", ",$flds);

        $sql = "INSERT INTO db_event_notif.db_course SET $fields";
        $this->conn->Execute($sql);

        $_SESSION[SYSTEM_SESSION]['eMsg']="Successfully Added.";
    }

    /**
     * Save Update
     *
     */
    function doSaveEdit(){
        $id = $_GET['edit'];

        $flds = array();
        foreach ($this->Data as $keyData => $valData) {
            $flds[] = "$keyData='$valData'";
        }
        $fields = implode(", ",$flds);

        $sql = "UPDATE db_event_notif.db_course SET $fields WHERE course_id=$id";
        $this->conn->Execute($sql);
        $_SESSION[SYSTEM_SESSION]['eMsg']="Successfully Updated.";
    }

    /**
     * Delete Record
     *
     * @param string $id_
     */
    function doDelete($id_ = ""){
        $sql = "DELETE FROM db_event_notif.db_course WHERE course_id=?";
        $this->conn->Execute($sql,array($id_));
        $_SESSION[SYSTEM_SESSION]['eMsg']="Successfully Deleted.";
    }

    /**
     * Get all the Table Listings
     *
     * @return array
     */
    function getTableList(){
        // Process the query string and exclude querystring named "p"
        if (!empty($_SERVER['QUERY_STRING'])) {
            $qrystr = explode("&",$_SERVER['QUERY_STRING']);
            foreach ($qrystr as $value) {
                $qstr = explode("=",$value);
                if ($qstr[0]!="p") {
                    $arrQryStr[] = implode("=",$qstr);
                }
            }
            $aQryStr = $arrQryStr;
            $aQryStr[] = "p=@@";
            $queryStr = implode("&",$aQryStr);
        }

        //bby: search module
        $qry = array();
        if (isset($_REQUEST['search_field'])) {

            // lets check if the search field has a value
            if (strlen($_REQUEST['search_field'])>0) {
                // lets assign the request value in a variable
                $search_field = MainBlock::BlockSQLInjection($_REQUEST['search_field']);

                // create a custom criteria in an array
                $qry[] = "(dco.course_name like '%$search_field%'
                            OR dco.course_shortname like '%$search_field%'
                            OR if(dco.is_active = 1, concat('Active'), concat('Not Active')) like '%$search_field%'
                            )";

            }
        }

        // put all query array into one criteria string
        $criteria = (count($qry)>0)?" where ".implode(" and ",$qry):"";

        $viewLink = "";
        $editLink = "<a href=\"?inpage=course&edit=',dco.course_id,'\" class=\"btn btn-sm btn-success\">EDIT";
        $delLink = "<a href=\"?inpage=course&delete=',dco.course_id,'\" onclick=\"return confirm(\'Are you sure, you want to delete?\');\" class=\"btn btn-flat btn-xs bg-orange\"><span class=\"glyphicon glyphicon-trash\"></span> DELETE</a>";

        // Sort field mapping
        $arrSortBy = array(
            "course_name"=>"dco.course_name"
        ,"course_shortname"=>"dco.course_shortname"
        ,"department_shortname"=>"dd.department_shortname"
        ,"campus_shortname"=>"dc.campus_shortname"
        ,"status_ref"=>"if(dco.is_active = 1, concat('Active'), concat('Not Active'))"
        );

        if(isset($_GET['sortby'])){
            $strOrderBy = " order by ".$arrSortBy[$_GET['sortby']]." ".$_GET['sortof'];
        }

        $sql = "SELECT dco.*, dd.department_shortname, dc.campus_shortname, if(dco.is_active = 1, concat('Active'), concat('Not Active')) as status_ref, concat('$editLink') as viewdata
                FROM db_event_notif.db_course dco
                LEFT JOIN db_event_notif.db_department dd on dco.department_id = dd.department_id
                LEFT JOIN db_event_notif.db_campus dc on dd.campus_id = dc.campus_id
                $criteria
                $strOrderBy
                ";

        $sqlcount = "SELECT count(*) as mycount FROM db_event_notif.db_course order by course_id $criteria";

        $arrFields = array(
            "course_name"=>"Name",
            "course_shortname"=>"Code Name",
            "department_shortname"=>"Department",
            "campus_shortname"=>"Campus",
            "status_ref"=>"Status",
            "viewdata"=>"&nbsp;"
        );

        $arrAttribs = array(
            "course_name"=>"style=\"vertical-align: middle;\""
        ,"course_shortname"=>"style=\"vertical-align: middle;\""
        ,"department_shortname"=>"style=\"vertical-align: middle;\""
        ,"campus_shortname"=>"style=\"vertical-align: middle;\""
        ,"status_ref"=>"style=\"vertical-align: middle;\""
        ,"viewdata"=>" style=\"white-space: nowrap;width: 1px\""
        );

        $tblDisplayList = new clsTableList($this->conn);
        $tblDisplayList->arrFields = $arrFields;
        $tblDisplayList->paginator->linkPage = "?$queryStr";
        $tblDisplayList->sqlAll = $sql;
        $tblDisplayList->sqlCount = $sqlcount;

        return $tblDisplayList->getTableList($arrAttribs);
    }

    function updateStatus($id){
        $sql = "SELECT * FROM db_event_notif.db_course WHERE course_id = '{$id}'";
        $rsResult = $this->conn->Execute($sql);
        if(!$rsResult->EOF){
            if($rsResult->fields['is_active'] == 1){
                $flds[] = "is_active = 0";
            }else{
                $flds[] = "is_active = 1";
            }
            $fields = implode(", ",$flds);
            $sqlUpdate = "UPDATE db_event_notif.db_course SET $fields WHERE course_id = '{$id}'";
            $this->conn->Execute($sqlUpdate);
        }
    }

    function getDepartmentList(){
        $sql = "SELECT * FROM db_event_notif.db_department WHERE is_active = 1";
        $rsResult = $this->conn->Execute($sql);
        while(!$rsResult->EOF){
            $arrResult[] = $rsResult->fields;
            $rsResult->MoveNext();
        }
        return $arrResult;
    }

}


?>
