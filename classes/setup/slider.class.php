<?php

class clsSliderSetup{

    var $conn;
    var $fieldMap;
    var $Data;

    /**ss
     * Class Constructor
     *
     * @param object $dbconn_
     * @return clsSliderSetup object
     */
    function clsSliderSetup($dbconn_ = null){
        $this->conn =& $dbconn_;
        $this->fieldMap = array(
            "si_name" => "si_name",
            "si_desc" => "si_desc",
            "si_ordinal" => "si_ordinal",
        );
    }

    /**
     * Get the records from the database
     *
     * @param string $id_
     * @return array
     */
    function dbFetch($id_ = ""){
        $sql = "SELECT * FROM db_event_notif.db_slider_img WHERE si_id = ?";
        $rsResult = $this->conn->Execute($sql,array($id_));
        if(!$rsResult->EOF){
            return $rsResult->fields;
        }
    }
    /**
     * Populate array parameters to Data Variable
     *
     * @param array $pData_
     * @return bool
     */
    function doPopulateData($pData_ = array()){
        if(count($pData_)>0){
            foreach ($this->fieldMap as $key => $value) {
                $this->Data[$key] = $pData_[$value];
            }
            return true;
        }
        return false;
    }

    /**
     * Validation function
     *
     * @param array $pData_
     * @return bool
     */
    function doValidateData($pData_ = array()){
        $isValid = true;

//		$isValid = false;

        return $isValid;
    }

    /**
     * Save New
     *
     */
    function doSaveAdd(){
        $flds = array();
        foreach ($this->Data as $keyData => $valData) {
            $flds[] = "$keyData='$valData'";
        }
        $fields = implode(", ",$flds);

        $sql = "INSERT INTO db_event_notif.db_slider_img SET $fields";
        $this->conn->Execute($sql);

        $lastInsertId = $this->conn->Insert_ID();

        if($this->imageExtract($_FILES['si_img'])){
            $flds2[] = "si_img = '{$this->imageExtract($_FILES['si_img'])}'";
            $fields2 = implode(", ", $flds2);
            $sqlUpdate = "UPDATE db_event_notif.db_slider_img SET $fields2 WHERE si_id = '{$lastInsertId}'";
            $this->conn->Execute($sqlUpdate);
        }

        $_SESSION[SYSTEM_SESSION]['eMsg']="Successfully Added.";
    }

    /**
     * Save Update
     *
     */
    function doSaveEdit(){
        $id = $_GET['edit'];

        $flds = array();
        foreach ($this->Data as $keyData => $valData) {
            $flds[] = "$keyData='$valData'";
        }
        $fields = implode(", ",$flds);

        $sql = "UPDATE db_event_notif.db_slider_img SET $fields WHERE si_id=$id";
        $this->conn->Execute($sql);


        if($this->imageExtract($_FILES['si_img'])){
            $sqlUpdate = "UPDATE db_event_notif.db_slider_img SET si_img = '{$this->imageExtract($_FILES['si_img'])}' WHERE si_id = '{$id}'";
            $this->conn->Execute($sqlUpdate);
        }

        MainBlock::getDefaultSiteSettings($this->conn);

        $_SESSION[SYSTEM_SESSION]['eMsg']="Successfully Updated.";
    }

    /**
     * Delete Record
     *
     * @param string $id_
     */
    function doDelete($id_ = ""){
        $sql = "DELETE FROM db_event_notif.db_slider_img WHERE si_id=?";
        $this->conn->Execute($sql,array($id_));
        $_SESSION[SYSTEM_SESSION]['eMsg']="Successfully Deleted.";
    }

    /**
     * Get all the Table Listings
     *
     * @return array
     */
    function getTableList(){
        // Process the query string and exclude querystring named "p"
        if (!empty($_SERVER['QUERY_STRING'])) {
            $qrystr = explode("&",$_SERVER['QUERY_STRING']);
            foreach ($qrystr as $value) {
                $qstr = explode("=",$value);
                if ($qstr[0]!="p") {
                    $arrQryStr[] = implode("=",$qstr);
                }
            }
            $aQryStr = $arrQryStr;
            $aQryStr[] = "p=@@";
            $queryStr = implode("&",$aQryStr);
        }

        //bby: search module
        $qry = array();
        if (isset($_REQUEST['search_field'])) {

            // lets check if the search field has a value
            if (strlen($_REQUEST['search_field'])>0) {
                // lets assign the request value in a variable
                $search_field = MainBlock::BlockSQLInjection($_REQUEST['search_field']);

                // create a custom criteria in an array
                $qry[] = "(si_name like '%$search_field%'
                            OR si_desc like '%$search_field%'
                            OR if(is_active = 1, concat('Active'), concat('Not Active')) like '%$search_field%'
                            )";

            }
        }

        // put all query array into one criteria string
        $criteria = (count($qry)>0)?" where ".implode(" and ",$qry):"";

        $viewLink = "";
        $editLink = "<a href=\"?inpage=site&edit=',si_id,'\" class=\"btn btn-sm btn-success\">EDIT";
        $delLink = "<a href=\"?inpage=site&delete=',si_id,'\" onclick=\"return confirm(\'Are you sure, you want to delete?\');\" class=\"btn btn-sm bg-orange\"><span class=\"glyphicon glyphicon-trash\"></span> DELETE</a>";
        $img = "<img src=\"?inpage=site&image=',si_id,'\" style=\"width: 100%;\">";

        // Sort field mapping
        $arrSortBy = array(
            "si_name"=>"si_name",
            "status_ref"=>"if(is_active = 1, concat('Active'), concat('Not Active'))"
        );

        if(isset($_GET['sortby'])){
            $strOrderBy = " order by ".$arrSortBy[$_GET['sortby']]." ".$_GET['sortof'];
        }

        $sql = "SELECT *, concat('$editLink', '$delLink') as viewdata, concat('$img') as image, if(is_active = 1, concat('Active'), concat('Not Active')) as status_ref
                FROM db_event_notif.db_slider_img
                $criteria
                $strOrderBy
                ";

        $sqlcount = "SELECT count(*) as mycount FROM db_event_notif.db_slider_img order by si_id $criteria";

        $arrFields = array(
            "image"=>"&nbsp;",
            "si_name"=>"Title",
            "status_ref"=>"Status",
            "viewdata"=>"&nbsp;"
        );

        $arrAttribs = array(
            "image"=>"style=\"width: 20%; vertical-align: middle;\""
        ,"si_name"=>"style=\"vertical-align: top;\""
        ,"status_ref"=>"style=\"white-space: nowrap; width: 1px; text-align: center;\""
        ,"viewdata"=>" style=\"white-space: nowrap;width: 1px\""
        );

        $tblDisplayList = new clsTableList($this->conn);
        $tblDisplayList->arrFields = $arrFields;
        $tblDisplayList->paginator->linkPage = "?$queryStr";
        $tblDisplayList->sqlAll = $sql;
        $tblDisplayList->sqlCount = $sqlcount;

        return $tblDisplayList->getTableList($arrAttribs);
    }

    function updateStatus($id){
        $sql = "SELECT * FROM db_event_notif.db_slider_img WHERE si_id = '{$id}'";
        $rsResult = $this->conn->Execute($sql);
        if(!$rsResult->EOF){
            if($rsResult->fields['is_active'] == 1){
                $flds[] = "is_active = 0";
            }else{
                $flds[] = "is_active = 1";
            }
            $fields = implode(", ",$flds);
            $sqlUpdate = "UPDATE db_event_notif.db_slider_img SET $fields WHERE si_id = '{$id}'";
            $this->conn->Execute($sqlUpdate);
        }
    }

    function getPhoto($gData){
        $sql = "SELECT si_img FROM db_event_notif.db_slider_img WHERE si_id = '{$gData['image']}'";
        $rsResult = $this->conn->Execute($sql);

        if(!$rsResult->EOF){
            if (strlen($rsResult->fields['si_img'])>0){
                return $rsResult->fields['si_img'];
            }else{
                return file_get_contents(SYSCONFIG_ROOT_PATH."/components/images/empty_logo_square.png");
            }
        }else{
            return file_get_contents(SYSCONFIG_ROOT_PATH."/components/images/empty_logo_square.png");
        }
    }

    function imageExtract($image_prop){
        if ($image_prop['error']!=4) {
            $valData=addslashes(file_get_contents($image_prop['tmp_name']));
            if ($valData==""){
                return false;
            }else{
                return $valData;
            }
        }else{
            return false;
        }
    }

    function getList(){
        $sql = "SELECT *
                FROM db_event_notif.db_slider_img
                ORDER BY si_ordinal ASC, si_name ASC
                ";
        $rsResult = $this->conn->Execute($sql);
        while(!$rsResult->EOF){
            $arrResult[] = $rsResult->fields;
            $rsResult->MoveNext();
        }
        return $arrResult;
    }

}


?>
