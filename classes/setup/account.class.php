<?php

class clsAccountSetup{

    var $conn;
    var $fieldMap;
    var $Data;

    /**ss
     * Class Constructor
     *
     * @param object $dbconn_
     * @return clsAccountSetup object
     */
    function clsAccountSetup($dbconn_ = null){
        $this->conn =& $dbconn_;
        $this->fieldMap = array(
            "user_name" => "user_name",
            "user_pass_raw" => "user_pass_raw",
            "user_pass_md5" => "user_pass_md5"
        );
    }

    /**
     * Get the records from the database
     *
     * @param string $id_
     * @return array
     */
    function dbFetch($id_ = ""){
        $sql = "SELECT * FROM db_event_notif.db_user WHERE user_id = ?";
        $rsResult = $this->conn->Execute($sql,array($id_));
        if(!$rsResult->EOF){
            return $rsResult->fields;
        }
    }
    /**
     * Populate array parameters to Data Variable
     *
     * @param array $pData_
     * @return bool
     */
    function doPopulateData($pData_ = array()){
        if(count($pData_)>0){
            foreach ($this->fieldMap as $key => $value) {
                $this->Data[$key] = $pData_[$value];
            }
            return true;
        }
        return false;
    }

    /**
     * Validation function
     *
     * @param array $pData_
     * @return bool
     */
    function doValidateData($pData_ = array()){
        $isValid = true;

//		$isValid = false;

        return $isValid;
    }

    /**
     * Save New
     *
     */
    function doSaveAdd(){
        $flds = array();
        foreach ($this->Data as $keyData => $valData) {
            if($keyData == 'user_pass_md5'){
                $md5_pass = md5($this->Data['user_pass_raw']);
                $flds[] = "$keyData='$md5_pass'";
            }else{
                $flds[] = "$keyData='$valData'";
            }
        }
        $fields = implode(", ",$flds);

        $sql = "INSERT INTO db_event_notif.db_user SET $fields";
        $this->conn->Execute($sql);

        $_SESSION[SYSTEM_SESSION]['eMsg']="Successfully Added.";
    }

    /**
     * Save Update
     *
     */
    function doSaveEdit(){
        $id = $_GET['edit'];

        $flds = array();
        foreach ($this->Data as $keyData => $valData) {
            if($keyData == 'user_pass_md5'){
                $md5_pass = md5($this->Data['user_pass_raw']);
                $flds[] = "$keyData='$md5_pass'";
            }else{
                $flds[] = "$keyData='$valData'";
            }
        }
        $fields = implode(", ",$flds);

        $sql = "UPDATE db_event_notif.db_user SET $fields WHERE user_id=$id";
        $this->conn->Execute($sql);
        $_SESSION[SYSTEM_SESSION]['eMsg']="Successfully Updated.";
    }

    /**
     * Delete Record
     *
     * @param string $id_
     */
    function doDelete($id_ = ""){
        $sql = "DELETE FROM db_event_notif.db_user WHERE user_id=?";
        $this->conn->Execute($sql,array($id_));
        $_SESSION[SYSTEM_SESSION]['eMsg']="Successfully Deleted.";
    }

    /**
     * Get all the Table Listings
     *
     * @return array
     */
    function getTableList(){
        // Process the query string and exclude querystring named "p"
        if (!empty($_SERVER['QUERY_STRING'])) {
            $qrystr = explode("&",$_SERVER['QUERY_STRING']);
            foreach ($qrystr as $value) {
                $qstr = explode("=",$value);
                if ($qstr[0]!="p") {
                    $arrQryStr[] = implode("=",$qstr);
                }
            }
            $aQryStr = $arrQryStr;
            $aQryStr[] = "p=@@";
            $queryStr = implode("&",$aQryStr);
        }

        //bby: search module
        $qry = array();
        if (isset($_REQUEST['search_field'])) {

            // lets check if the search field has a value
            if (strlen($_REQUEST['search_field'])>0) {
                // lets assign the request value in a variable
                $search_field = MainBlock::BlockSQLInjection($_REQUEST['search_field']);

                // create a custom criteria in an array
                $qry[] = "(user_name like '%$search_field%'
                            OR if(is_active = 1, concat('Active'), concat('Not Active')) like '%$search_field%'
                            )";

            }
        }

        // put all query array into one criteria string
        $criteria = (count($qry)>0)?" where ".implode(" and ",$qry):"";

        $viewLink = "";
        $editLink = "<a href=\"?inpage=account&edit=',user_id,'\" class=\"btn btn-sm btn-success\">EDIT";
        $delLink = "<a href=\"?inpage=account&delete=',user_id,'\" onclick=\"return confirm(\'Are you sure, you want to delete?\');\" class=\"btn btn-flat btn-xs bg-orange\"><span class=\"glyphicon glyphicon-trash\"></span> DELETE</a>";

        // Sort field mapping
        $arrSortBy = array(
            "user_name"=>"user_name"
            ,"status_ref"=>"if(is_active = 1, concat('Active'), concat('Not Active'))"
        );

        if(isset($_GET['sortby'])){
            $strOrderBy = " order by ".$arrSortBy[$_GET['sortby']]." ".$_GET['sortof'];
        }

        $sql = "SELECT *, if(is_active = 1, concat('Active'), concat('Not Active')) as status_ref, concat('$editLink') as viewdata
                FROM db_event_notif.db_user
                $criteria
                $strOrderBy
                ";

        $sqlcount = "SELECT count(*) as mycount FROM db_event_notif.db_user order by user_id $criteria";

        $arrFields = array(
            "user_name"=>"Username",
            "status_ref"=>"Status",
            "viewdata"=>"&nbsp;"
        );

        $arrAttribs = array(
            "user_name"=>"style=\"vertical-align: middle;\""
        ,"status_ref"=>"style=\"vertical-align: middle;\""
        ,"viewdata"=>" style=\"white-space: nowrap;width: 1px\""
        );

        $tblDisplayList = new clsTableList($this->conn);
        $tblDisplayList->arrFields = $arrFields;
        $tblDisplayList->paginator->linkPage = "?$queryStr";
        $tblDisplayList->sqlAll = $sql;
        $tblDisplayList->sqlCount = $sqlcount;

        return $tblDisplayList->getTableList($arrAttribs);
    }

    function updateStatus($id){
        $sql = "SELECT * FROM db_event_notif.db_user WHERE user_id = '{$id}'";
        $rsResult = $this->conn->Execute($sql);
        if(!$rsResult->EOF){
            if($rsResult->fields['is_active'] == 1){
                $flds[] = "is_active = 0";
            }else{
                $flds[] = "is_active = 1";
            }
            $fields = implode(", ",$flds);
            $sqlUpdate = "UPDATE db_event_notif.db_user SET $fields WHERE user_id = '{$id}'";
            $this->conn->Execute($sqlUpdate);
        }
    }

}


?>
