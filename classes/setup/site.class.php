<?php

class clsSiteSetup{

    var $conn;
    var $fieldMap;
    var $Data;

    /**ss
     * Class Constructor
     *
     * @param object $dbconn_
     * @return clsSiteSetup object
     */
    function clsSiteSetup($dbconn_ = null){
        $this->conn =& $dbconn_;
        $this->fieldMap = array(
            "site_title" => "site_title",
            "site_desc" => "site_desc",
        );
    }

    /**
     * Get the records from the database
     *
     * @param string $id_
     * @return array
     */
    function dbFetch($id_ = ""){
        $sql = "SELECT * FROM db_event_notif.db_site WHERE site_id = ?";
        $rsResult = $this->conn->Execute($sql,array($id_));
        if(!$rsResult->EOF){
            return $rsResult->fields;
        }
    }
    /**
     * Populate array parameters to Data Variable
     *
     * @param array $pData_
     * @return bool
     */
    function doPopulateData($pData_ = array()){
        if(count($pData_)>0){
            foreach ($this->fieldMap as $key => $value) {
                $this->Data[$key] = $pData_[$value];
            }
            return true;
        }
        return false;
    }

    /**
     * Validation function
     *
     * @param array $pData_
     * @return bool
     */
    function doValidateData($pData_ = array()){
        $isValid = true;

//		$isValid = false;

        return $isValid;
    }

    /**
     * Save New
     *
     */
    function doSaveAdd(){
        $flds = array();
        foreach ($this->Data as $keyData => $valData) {
            $flds[] = "$keyData='$valData'";
        }
        $fields = implode(", ",$flds);

        $sql = "INSERT INTO db_event_notif.db_site SET $fields";
        $this->conn->Execute($sql);

        $lastInsertId = $this->conn->Insert_ID();

        $sqlGetInfo = "SELECT COUNT(*) FROM db_event_notif.db_site WHERE is_active = 1";
        $rsGI = $this->conn->Execute($sqlGetInfo);
        if($rsGI->fields['counter'] <= 0){
            $flds2[] = "is_active = 1";
        }

        if($this->imageExtract($_FILES['site_logo'])){
            $flds2[] = "site_logo = '{$this->imageExtract($_FILES['site_logo'])}'";
        }

        $fields2 = implode(", ", $flds2);
        $sqlUpdate = "UPDATE db_event_notif.db_site SET $fields2 WHERE site_id = '{$lastInsertId}'";
        $this->conn->Execute($sqlUpdate);

        MainBlock::getDefaultSiteSettings($this->conn);

        $_SESSION[SYSTEM_SESSION]['eMsg']="Successfully Added.";
    }

    /**
     * Save Update
     *
     */
    function doSaveEdit(){
        $id = $_GET['edit'];

        $flds = array();
        foreach ($this->Data as $keyData => $valData) {
            $flds[] = "$keyData='$valData'";
        }
        $fields = implode(", ",$flds);

        $sql = "UPDATE db_event_notif.db_site SET $fields WHERE site_id=$id";
        $this->conn->Execute($sql);


        if($this->imageExtract($_FILES['site_logo'])){
            $sqlUpdate = "UPDATE db_event_notif.db_site SET site_logo = '{$this->imageExtract($_FILES['site_logo'])}' WHERE site_id = '{$id}'";
            $this->conn->Execute($sqlUpdate);
        }

        MainBlock::getDefaultSiteSettings($this->conn);

        $_SESSION[SYSTEM_SESSION]['eMsg']="Successfully Updated.";
    }

    /**
     * Delete Record
     *
     * @param string $id_
     */
    function doDelete($id_ = ""){
        $sql = "DELETE FROM db_event_notif.db_site WHERE site_id=?";
        $this->conn->Execute($sql,array($id_));
        $_SESSION[SYSTEM_SESSION]['eMsg']="Successfully Deleted.";
    }

    /**
     * Get all the Table Listings
     *
     * @return array
     */
    function getTableList(){
        // Process the query string and exclude querystring named "p"
        if (!empty($_SERVER['QUERY_STRING'])) {
            $qrystr = explode("&",$_SERVER['QUERY_STRING']);
            foreach ($qrystr as $value) {
                $qstr = explode("=",$value);
                if ($qstr[0]!="p") {
                    $arrQryStr[] = implode("=",$qstr);
                }
            }
            $aQryStr = $arrQryStr;
            $aQryStr[] = "p=@@";
            $queryStr = implode("&",$aQryStr);
        }

        //bby: search module
        $qry = array();
        if (isset($_REQUEST['search_field'])) {

            // lets check if the search field has a value
            if (strlen($_REQUEST['search_field'])>0) {
                // lets assign the request value in a variable
                $search_field = MainBlock::BlockSQLInjection($_REQUEST['search_field']);

                // create a custom criteria in an array
                $qry[] = "(site_title like '%$search_field%'
                            OR site_desc like '%$search_field%'
                            OR if(is_active = 1, concat('Active'), concat('Not Active')) like '%$search_field%'
                            )";

            }
        }

        // put all query array into one criteria string
        $criteria = (count($qry)>0)?" where ".implode(" and ",$qry):"";

        $viewLink = "";
        $editLink = "<a href=\"?inpage=site&edit=',site_id,'\" class=\"btn btn-sm btn-success\">EDIT";
        $delLink = "<a href=\"?inpage=site&delete=',site_id,'\" onclick=\"return confirm(\'Are you sure, you want to delete?\');\" class=\"btn btn-sm bg-orange\"><span class=\"glyphicon glyphicon-trash\"></span> DELETE</a>";
        $img = "<img src=\"?inpage=site&image=',site_id,'\" style=\"width: 100%;\">";

        // Sort field mapping
        $arrSortBy = array(
            "site_title"=>"site_title",
            "status_ref"=>"if(is_active = 1, concat('Active'), concat('Not Active'))"
        );

        if(isset($_GET['sortby'])){
            $strOrderBy = " order by ".$arrSortBy[$_GET['sortby']]." ".$_GET['sortof'];
        }

        $sql = "SELECT *, concat('$editLink', '$delLink') as viewdata, concat('$img') as image, if(is_active = 1, concat('Active'), concat('Not Active')) as status_ref
                FROM db_event_notif.db_site
                $criteria
                $strOrderBy
                ";

        $sqlcount = "SELECT count(*) as mycount FROM db_event_notif.db_site order by site_id $criteria";

        $arrFields = array(
            "image"=>"&nbsp;",
            "site_title"=>"Title",
            "status_ref"=>"Status",
            "viewdata"=>"&nbsp;"
        );

        $arrAttribs = array(
            "image"=>"style=\"width: 20%; vertical-align: middle;\""
            ,"site_title"=>"style=\"vertical-align: top;\""
            ,"status_ref"=>"style=\"white-space: nowrap; width: 1px; text-align: center;\""
            ,"viewdata"=>" style=\"white-space: nowrap;width: 1px\""
        );

        $tblDisplayList = new clsTableList($this->conn);
        $tblDisplayList->arrFields = $arrFields;
        $tblDisplayList->paginator->linkPage = "?$queryStr";
        $tblDisplayList->sqlAll = $sql;
        $tblDisplayList->sqlCount = $sqlcount;

        return $tblDisplayList->getTableList($arrAttribs);
    }

    function updateStatus($id){
        $sql = "SELECT * FROM db_event_notif.db_site WHERE site_id = '{$id}'";
        $rsResult = $this->conn->Execute($sql);
        if(!$rsResult->EOF){
            if($rsResult->fields['is_active'] == 1){
                $flds[] = "is_active = 0";
            }else{
                $flds[] = "is_active = 1";
            }
            $fields = implode(", ",$flds);
            $sqlUpdate = "UPDATE db_event_notif.db_site SET $fields WHERE site_id = '{$id}'";
            $this->conn->Execute($sqlUpdate);
        }
    }

    function getPhoto($gData){
        $sql = "SELECT site_logo FROM db_event_notif.db_site WHERE site_id = '{$gData['image']}'";
        $rsResult = $this->conn->Execute($sql);

        if(!$rsResult->EOF){
            if (strlen($rsResult->fields['site_logo'])>0){
                return $rsResult->fields['site_logo'];
            }else{
                return file_get_contents(SYSCONFIG_ROOT_PATH."/components/images/empty_logo_square.png");
            }
        }else{
            return file_get_contents(SYSCONFIG_ROOT_PATH."/components/images/empty_logo_square.png");
        }
    }

    function imageExtract($image_prop){
        if ($image_prop['error']!=4) {
            $valData=addslashes(file_get_contents($image_prop['tmp_name']));
            if ($valData==""){
                return false;
            }else{
                return $valData;
            }
        }else{
            return false;
        }
    }

}


?>
