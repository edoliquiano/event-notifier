<?php
/**
 * Grading Module Helper
 *
 * @author Arnold P. Orbista
 *
 */


class clsRegistrarMisc {

	function clsRegistrarMisc() {

	}

	/**/

    function getSyInfo($dbconn_ = null, $sy_id = null, $sy_from = null, $sy_to = null){

        if (is_null($dbconn_)) {
            return $retVal;
        }

        if ($sy_id != ''){
            $criteria = " where sy_id = '$sy_id'";
        }else{
            $criteria = " where sy_default = '1'";
        }

        if($sy_from != '' and $sy_to != ''){
            $criteria = " where sy_from = '$sy_from' and sy_to = '$sy_to'";
        }

        $sql = "SELECT sy_id,sy_name,sy_from,sy_to, CONCAT(sy_from,'-',sy_to) as school_year FROM db_event_notif.file_schoolyear $criteria";
        $rsResult = $dbconn_->Execute($sql);

        if(!$rsResult->EOF){
            $rsResult = $rsResult->fields;
            $sy_name = $rsResult->fields['sy_name'];
            $_SESSION[SYSTEM_SESSION]['yl_level']['sy_from'] = $rsResult['sy_from'];
            $_SESSION[SYSTEM_SESSION]['yl_level']['sy_to'] = $rsResult['sy_to'];
        }
        return $rsResult;
    }

    function getSchoolYears($dbconn_ = null, $limit_ = null){
	if (is_null($dbconn_)) {
            return $retVal;
        }

        if (is_null($limit_)) {
            $limit = "LIMIT 10";
        }else if($limit_ == 0){
            $limit = "";
        }else{
            $limit = "LIMIT $limit_";
        }

        $sql = "SELECT sy_id, sy_name, sy_from, sy_to FROM db_event_notif.file_schoolyear ORDER BY sy_from DESC $limit";
        $rsResult = $dbconn_->Execute($sql);

        $arrResult = array();
        while(!$rsResult->EOF){
            $arrResult[] = $rsResult->fields;
            $rsResult->MoveNext();
        }
        return $arrResult;
    }

    /**
     * code for trailings added nov. 16 2009 : dps commonwealth  DPS � COD � T0019
     *
     * @param unknown_type $dbconn_
     * @param unknown_type $stusertype_id
     * @param unknown_type $emp_id
     * @param unknown_type $action
     * @param unknown_type $trail_code // its in file_admission_trail_code
     * @return unknown
     */
    function doSaveTrails($dbconn_=null,$stusertype_id=null,$action=null,$trail_code=null){
        if (is_null($dbconn_)) {
            return $retVal;
        }

        $flds=array();
        $flds[]="fat_action_details='$action'";
        $flds[]="stud_applicant_id='$stusertype_id'";
        $flds[]="fat_modified_by='{$_SESSION[SYSTEM_SESSION]['user_data']['emp_id']}'";
        $flds[]="fat_addwho='{$_SESSION[SYSTEM_SESSION]['user_data']['user_name']}'";
        $flds[]="trail_id='$trail_code'";
        $flds[]="sy_id='{$_SESSION[SYSTEM_SESSION]['syinfo']['sy_id']}'";

        $fields = implode(", ",$flds);

        $sql = "insert into file_admission_trailings set $fields";
        $dbconn_->Execute($sql);

    }

    function getSelected_Table($dbconn_ = null, $sy_id = null, $use_default = null){
//        echo $sy_id;
        if (is_null($dbconn_)) {
            return $retVal;
        }

        if ($sy_id != ''){
            $criteria = " where sy_id = '$sy_id'";
        }else{
            $sy_id = $_SESSION[SYSTEM_SESSION]['syinfo']['sy_id'];
            $criteria = " where sy_id = '$sy_id'";
        }

        if($use_default != ''){
            $criteria = " where sy_default = '1'";
        }

        $sql = "SELECT concat(sy_from,'_',sy_to) as selected_table FROM db_event_notif.file_schoolyear $criteria";
        $rsResult = $dbconn_->Execute($sql);
//        echo "<br>";
        if(!$rsResult->EOF){
            return $rsResult = $rsResult->fields['selected_table'];
        }

    }

    function getSchoolYearCodes($dbconn_ = null){

        if (is_null($dbconn_)) {
            return $retVal;
        }

        $sql = "SELECT CONCAT(pyc_id) as psc_id,CONCAT(pyc_name) as psc_name FROM db_event_notif.payment_year_code where pyc_active = 10";
        $rsResult = $dbconn_->Execute($sql);

        $arrResult = array();
        while(!$rsResult->EOF){
            $arrResult[] = $rsResult->fields;
            $rsResult->MoveNext();
        }

        return $arrResult;
    }

    function getPaymentScheme($dbconn_ = null){

        if (is_null($dbconn_)) {
            return $retVal;
        }

        $qry = array();
        $qry[] = "ps_active = 10";
        $criteria = (count($qry)>0)?" where ".implode(" and ",$qry):"";

        $sql = "select * from db_event_notif.payment_scheme $criteria";
        $rsResult = $dbconn_->Execute($sql);

        $arrData = array();
        while(!$rsResult->EOF){
            $arrData[] = $rsResult->fields;
            $rsResult->MoveNext();
        }
        return $arrData;

    }

    function display_sequence($dbconn_ = null, $sy_id = null, $table_id = null, $sequence_for = null){

        if (is_null($dbconn_)) {
            return $retVal;
        }

        $sql = "select ds_id, MAX(sequence_no) as sequence_no
                from db_event_notif.display_sequence
                where sequence_for = '$sequence_for' and sy_id = '$sy_id'
                group by sequence_for
                ";

        $rsResult = $dbconn_->Execute($sql);

        $flds = array();
        if (!$rsResult->EOF){
            $rsResult = $rsResult->fields['sequence_no'];

            $sequence_no = $rsResult + 1;
            $flds[] = "table_id = '$table_id'";
            $flds[] = "sequence_no = '$sequence_no'";
            $flds[] = "sequence_for = '$sequence_for'";
            $flds[] = "sy_id = '$sy_id'";
            $fields = implode(",",$flds);

            $insert = "insert into db_event_notif.display_sequence set $fields";
            $dbconn_->Execute($insert);

            return $sequence_no;

        }else{

            $flds[] = "table_id = '$table_id'";
            $flds[] = "sequence_no = 1";
            $flds[] = "sequence_for = '$sequence_for'";
            $flds[] = "sy_id = '$sy_id'";
            $fields = implode(",",$flds);

            $insert = "insert into db_event_notif.display_sequence set $fields";
            $dbconn_->Execute($insert);

            return 1;

        }

    }

    function Student_Information($dbconn_ = null, $stud_applicant_id = null, $flds = array()){

        if (is_null($dbconn_)) {
            return $retVal;
        }

        $fields = implode(",",$flds);

        $qry = array();
        $qry[] = "stud_applicant_id = $stud_applicant_id";
        $criteria = (count($qry)>0)?" where ".implode(" and ",$qry):"";

        $sql = "select $fields
                from db_event_notif.student_applicant_information
                $criteria
                ";

        $rsResult = $dbconn_->Execute($sql);
        if (!$rsResult->EOF){
            return $rsResult = $rsResult->fields;
        }

    }

    function GradeLevel_Info($dbconn_ = null, $sy_id = null, $yl_id = null){

        if (is_null($dbconn_)) {
            return $retVal;
        }

        $selected_table = clsRegistrarMisc::getSelected_Table($dbconn_, $sy_id);

        $qry = array();
        $qry[] = "yl_id = $yl_id";
        $criteria = (count($qry)>0)?" where ".implode(" and ",$qry):"";

        $sql = "select *
                from db_event_notif.file_gradeyearlevel_$selected_table
                $criteria
                ";

        $rsResult = $dbconn_->Execute($sql);
        if (!$rsResult->EOF){
            return $rsResult = $rsResult->fields;
        }

    }

    function Section_Info($dbconn_ = null, $sy_id = null, $sec_id = null){

        if (is_null($dbconn_)) {
            return $retVal;
        }

        $selected_table = clsRegistrarMisc::getSelected_Table($dbconn_, $sy_id);

        $qry = array();
        $qry[] = "section_id = $sec_id";
        $criteria = (count($qry)>0)?" where ".implode(" and ",$qry):"";

        $sql = "select *
                from db_event_notif.file_section_$selected_table
                $criteria
                ";

        $rsResult = $dbconn_->Execute($sql);
        if (!$rsResult->EOF){
            return $rsResult = $rsResult->fields;
        }

    }

    function ModeOfPayment_Info($dbconn_ = null, $sy_id = null, $ps_id = null){

        if (is_null($dbconn_)) {
            return $retVal;
        }

        $qry = array();
        $qry[] = "ps_id = '$ps_id'";
        $criteria = (count($qry)>0)?" where ".implode(" and ",$qry):"";

        $sql = "select *
                from db_event_notif.payment_scheme
                $criteria";


        $rsResult = $dbconn_->Execute($sql);
        if (!$rsResult->EOF){
            return $rsResult = $rsResult->fields;
        }

    }

    function getGradeLevel($dbconn_ = null, $sy_id = null, $isactive = null){

        if (is_null($dbconn_)) {
            return $retVal;
        }

        $selected_table = clsRegistrarMisc::getSelected_Table($dbconn_, $sy_id);

        $qry = array();
        if($isactive == '1'){
            $qry[] ="yl_isactive = 1";
        }
        $criteria = (count($qry)>0)?" where ".implode(" and ",$qry):"";

        $sql = "SELECT *
                FROM db_event_notif.file_gradeyearlevel_$selected_table
                $criteria
                ORDER BY yl_ordinal ASC";
        $rsResult = $dbconn_->Execute($sql);

        $arrData = array();
        while(!$rsResult->EOF){
            $arrData[] = $rsResult->fields;
            $rsResult->MoveNext();
        }

        return $arrData;

    }

    function update_permit($dbconn_ = null, $sy_id = null){

        if (is_null($dbconn_)) {
            return $retVal;
        }

        $selected_table = clsRegistrarMisc::getSelected_Table($dbconn_, $sy_id);

        $sql = "update db_event_notif.file_permit_to_enroll_$selected_table a
                     set a.grading = 1
                     ";

        $rsResult = $dbconn_->Execute($sql);

        $qry = array();
        $qry[] = "a.stud_applicant_id = b.stud_applicant_id";
        $qry[] = "b.sg_action_taken <= 'Failed'";
        $criteria = (count($qry)>0)?" where ".implode(" and ",$qry):"";

        $sql_ = "SELECT DISTINCT TABLE_NAME
                FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name like '%grd_subject_grades%'
                order by table_name desc";

        $rsResult_ = $dbconn_->Execute($sql_);

        while(!$rsResult_->EOF){

            $sql = "update db_event_notif.file_permit_to_enroll_$selected_table a,
                        dps_grade_db.{$rsResult_->fields['TABLE_NAME']} b
                    set a.grading = 0
                    $criteria
                    ";
            $rsResult = $dbconn_->Execute($sql);

            $rsResult_->MoveNext();
        }

    }
}
?>