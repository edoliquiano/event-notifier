<?php

class AppAuth {

    var $userID;
    var $userType;
    var $userName;
    var $conn;
    var $Data;

    var $error;

    function AppAuth(&$conn) {
        $this->conn = $conn;
        $this->sessionID = $_COOKIE[session_name()];
        $this->authTime = dDate::getDate("Y-m-d H:i:s",dDate::getTime());
    }

    /**
     * @param string $user
     * @param string $pass
     * @return int the user's member_id
     * @static
     */
    function doAuth($user,$pass) {
        $user = mysql_escape_string(addslashes($user));
        $pass = mysql_escape_string(addslashes($pass));

        $sql = "SELECT du.*, dut.usertype_name
                FROM db_event_notif.db_user du
                INNER JOIN db_event_notif.db_usertype dut
                WHERE du.user_name = ?
                AND du.user_pass_md5 = ?
                ";
        $rsResult = $this->conn->Execute($sql, array($user,$pass));
        if($rsResult->EOF){
            $this->error = 404;
            return false;
        }

        $this->userID = $rsResult->fields['user_id'];
        $this->userType = $rsResult->fields['usertype_name'];
        $this->userName = $rsResult->fields['user_name'];

        $this->Data = $rsResult->fields;

        return true;
    }

    function getModules($dbconn_ = null,$arrMenu_ = array(), $isChild_ = false, $level = 0){
        if(count($arrMenu_) > 0){
            $arrCtr = 0;
            foreach ($arrMenu_ as $key => $value) {
                $sql = "select * from db_modules appmod
				inner join db_usertype_access auta on appmod.module_id = auta.module_id
				inner join db_user au on au.usertype_id = auta.usertype_id
				where appmod.module_status=1 and appmod.module_parent=? and auta.usertype_name=? and au.user_id=?
				order by appmod.module_ord asc";
                $rsResult = $dbconn_->Execute($sql,array($value['module_id'],$_SESSION[SYSTEM_SESSION]['usertype_name'],$_SESSION[SYSTEM_SESSION]['user_id']));

                if($isChild_ && $level > 0)
                    $mnuData .= ",";

                $mnuIcon = empty($value['module_icon'])?"null":"'$value[module_icon]'";
                $mnuLink = empty($value['module_link'])?"null":"'$value[module_link]'";

                $mnuData .= "[$mnuIcon, '$value[module_name]', $mnuLink, null, 'P$value[module_id]'";

                $arrMenuIn = array();
                while(!$rsResult->EOF){
                    $arrMenuIn[] = $rsResult->fields;
                    $rsResult->MoveNext();
                }
                if(count($arrMenuIn) > 0){
                    $mnuData .= $this->getModules($dbconn_,$arrMenuIn,true,$level + 1);
                }
                $mnuData .= "]";
                if(!$isChild_ && (count($arrMenu_)-1) > $arrCtr++)
                    $mnuData .= ",\n";
            }

        }
        return "$mnuData";
    }

    function getMenuModules($paramCriteria_ = null, &$paramConn_){
        $rsUserMenu = $paramConn_->Execute($paramCriteria_,array($_SESSION[SYSTEM_SESSION]['usertype_name'],$_SESSION[SYSTEM_SESSION]['user_id']));
        $arrMainMenu = array();
        while(!$rsUserMenu->EOF){
            $arrMainMenu[] = $rsUserMenu->fields;
            $rsUserMenu->MoveNext();
        }
        return $arrMainMenu;
    }

    function getModules2($dbconn_ = null,$arrMenu_ = array(), $isChild_ = false, $level = 0, $mnuModSql_ = ""){
        if(count($arrMenu_) > 0){
            $arrCtr = 0;
            foreach ($arrMenu_ as $key => $value) {
                $sql = $mnuModSql_;
                $rsResult = $dbconn_->Execute($sql,array($value['module_id'],$_SESSION[SYSTEM_SESSION]['usertype_name'],$_SESSION[SYSTEM_SESSION]['user_id']));

                $li_parent_class = "";
                $li_pull_right = "";

                $mnuData .= "";

                $mnuIcon = empty($value['module_icon'])?'<i class="fa fa-circle-thin"></i>':'<i class="fa '.$value['module_icon'].'"></i>';
                $mnuLink = empty($value['module_link'])?"#":$value['module_link'];

                $arrMenuIn = array();
                while(!$rsResult->EOF){
                    $arrMenuIn[] = $rsResult->fields;
                    $rsResult->MoveNext();
                }

                (count($arrMenuIn) > 0) ? $li_parent_class .= "" : $li_parent_class .= "";

                (count($arrMenuIn) > 0) ? $li_pull_right = "<span class=\"fa fa-chevron-down\"></span>" : "";

                $mnuData .= '<li '.$li_parent_class.'> <a href="'.$mnuLink.'">'.$mnuIcon.'<span>'. $value[module_name].'</span>'.$li_pull_right.'</a>';

                if(count($arrMenuIn) > 0){
                    $mnuData .= '<ul class="nav child_menu">'.$this->getModules2($dbconn_,$arrMenuIn,true,$level + 1,$mnuModSql_).'</ul>';
                }else{
                    $mnuData .= '<ul class="nav child_menu">'.$this->getModules2($dbconn_,$arrMenuIn,false,$level + 1,$mnuModSql_).'</ul>';
                }
                $mnuData .= "</li>";
                if(!$isChild_ && (count($arrMenu_)-1) > $arrCtr++)
                    $mnuData .= "\n";
            }

        }
        return "$mnuData";
    }

    function getModulesTop($dbconn_ = null,$arrMenu_ = array(), $isChild_ = false, $level = 0, $mnuModSql_ = ""){
        if(count($arrMenu_) > 0){
            $arrCtr = 0;
            foreach ($arrMenu_ as $key => $value) {
                $sql = $mnuModSql_;
                $rsResult = $dbconn_->Execute($sql,array($value['module_id'],$_SESSION[SYSTEM_SESSION]['usertype_name'],$_SESSION[SYSTEM_SESSION]['user_id']));

                $li_parent_class = "";
                $li_pull_right = "";

                $mnuData .= "";

//                <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Tooltip bottom">Tooltip bottom</button>

                $mnuIcon = empty($value['module_icon'])?'<i class="fa fa-circle-thin"></i>':'<i class="fa '.$value['module_icon'].'"></i>';
                $mnuLink = empty($value['module_link'])?"#":$value['module_link'];

                $arrMenuIn = array();
                while(!$rsResult->EOF){
                    $arrMenuIn[] = $rsResult->fields;
                    $rsResult->MoveNext();
                }

                (count($arrMenuIn) > 0) ? $li_parent_class .= "" : $li_parent_class .= "";

                (count($arrMenuIn) > 0) ? $li_pull_right = "<span class=\"fa fa-chevron-down\"></span>" : "";

                $mnuData .= '<li '.$li_parent_class.' data-toggle="tooltip" data-placement="bottom" title="'.$value['module_name'].'"> <a href="'.$mnuLink.'">'.$mnuIcon.'<span></span>'.$li_pull_right.'</a>';

                if(count($arrMenuIn) > 0){
                    $mnuData .= '<ul class="nav child_menu">'.$this->getModules2($dbconn_,$arrMenuIn,true,$level + 1,$mnuModSql_).'</ul>';
                }else{
                    $mnuData .= '<ul class="nav child_menu">'.$this->getModules2($dbconn_,$arrMenuIn,false,$level + 1,$mnuModSql_).'</ul>';
                }
                $mnuData .= "</li>";
                if(!$isChild_ && (count($arrMenu_)-1) > $arrCtr++)
                    $mnuData .= "\n";
            }

        }
        return "$mnuData";
    }

}

?>