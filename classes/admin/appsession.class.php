<?php

class AppSession {

    function save() {
        $_SESSION[SYSTEM_SESSION] = $this->data;
    }

    function load() {
        if ( isset($_SESSION[SYSTEM_SESSION]) ) {
            $this->data = $_SESSION[SYSTEM_SESSION];
        } else {
            $this->AppSession();
        }
    }

    function check($params) {

        // check if login required
        if ( @$params['login_required'] ) {
            // check if logged_in
            if ( isset($_SESSION[SYSTEM_SESSION]['user_id']) && !empty($_SESSION[SYSTEM_SESSION]['user_id']) ){
                AppSession::checkIdle();
            } else {
                // access is denied to non-logged in user
                header('Location: index.php?inpage=logout');
                exit;
            }
        }

        // check user type required
        if ( (@$params['require_usertype'])) {
            if ( $params['require_usertype'] != AppSession::getUserItem('wise_membertype') )	{
                // access is denied to non-logged in user
                header('Location: index.php?inpage=logout');
                exit;
            }
        }
    }

    function create($params) {
        $_SESSION[SYSTEM_SESSION]['timer'] = time();

        foreach( $params['newsession'] as $nkey => $nvalue ) {
            $_SESSION[SYSTEM_SESSION][$nkey] = $nvalue;
        }

    }

    function reload($params) {
        $_SESSION[SYSTEM_SESSION]['timer'] = time();

        foreach( $params['newsession'] as $nkey => $nvalue ) {
            $_SESSION[SYSTEM_SESSION][$nkey] = $nvalue;
        }

    }

    function destroy() {
        unset($_SESSION[SYSTEM_SESSION]);
    }

    function checkIdle() {
        if ( isset($_SESSION[SYSTEM_SESSION]['timer'])) {
            if ( time() - $_SESSION[SYSTEM_SESSION]['timer'] > APPCONFIG_IDLE_TIMEOUT ) {
                // forward to error page indicating timeout/expired
                //AppSession::destroy();
                header('Location: index.php?inpage=logout&out=1');
                exit;
            }
        } else {
            if ( @$params['login_required'] ) {
                // timer is not set, access is denied
                //AppSession::destroy();
                header('Location: index.php?inpage=logout');
                exit;
            }
        }
    }

    function getSessionItem($name) {
        return $_SESSION[SYSTEM_SESSION][$name];
    }

    function getUserItem($name) {
        return $_SESSION[SYSTEM_SESSION]['user_data'][$name];
    }

    function start() {
        session_start();
    }

}



?>