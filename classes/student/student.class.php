<?php

class clsStudentList{

    var $conn;
    var $fieldMap;
    var $Data;

    /**ss
     * Class Constructor
     *
     * @param object $dbconn_
     * @return clsStudentList object
     */
    function clsStudentList($dbconn_ = null){
        $this->conn =& $dbconn_;
        $this->fieldMap = array(
            "student_gname" => "student_gname",
            "student_mname" => "student_mname",
            "student_lname" => "student_lname",
            "student_address" => "student_address",
            "student_mobile" => "student_mobile"
        );
    }

    /**
     * Get the records from the database
     *
     * @param string $id_
     * @return array
     */
    function dbFetch($id_ = ""){
        $sql = "SELECT * FROM db_event_notif.db_student WHERE student_id = ?";
        $rsResult = $this->conn->Execute($sql,array($id_));
        if(!$rsResult->EOF){
            return $rsResult->fields;
        }
    }
    /**
     * Populate array parameters to Data Variable
     *
     * @param array $pData_
     * @return bool
     */
    function doPopulateData($pData_ = array()){
        if(count($pData_)>0){
            foreach ($this->fieldMap as $key => $value) {
                $this->Data[$key] = $pData_[$value];
            }
            return true;
        }
        return false;
    }

    /**
     * Validation function
     *
     * @param array $pData_
     * @return bool
     */
    function doValidateData($pData_ = array()){
        $isValid = true;

        if($pData_['student_gname'] == '' || empty($pData_['student_gname'])){
            $_SESSION[SYSTEM_SESSION]['eMsg'][] = 'Given Name is required';
            $isValid = false;
        }

        if($pData_['student_lname'] == '' || empty($pData_['student_lname'])){
            $_SESSION[SYSTEM_SESSION]['eMsg'][] = 'Last Name is required';
            $isValid = false;
        }

//		$isValid = false;

        return $isValid;
    }

    /**
     * Save New
     *
     */
    function doSaveAdd(){
        $flds = array();
        foreach ($this->Data as $keyData => $valData) {
            $flds[] = "$keyData='$valData'";
        }
        $fields = implode(", ",$flds);

        $sql = "INSERT INTO db_event_notif.db_student SET $fields";
        $this->conn->Execute($sql);


        $student_id = $this->conn->Insert_ID();

        if($_POST['course_id'] != ''){
            $sqlGetCourseInfo = "SELECT dd.*
                                FROM db_event_notif.db_course dc
                                INNER JOIN db_event_notif.db_department dd on dc.department_id = dd.department_id
                                WHERE dc.course_id = '{$_POST['course_id']}'
                                ";
            $rsCI = $this->conn->Execute($sqlGetCourseInfo);
            $sqlInsert = "INSERT INTO db_event_notif.db_student_rel SET student_id = '{$student_id}', campus_id = '{$rsCI->fields['campus_id']}', department_id = '{$rsCI->fields['department_id']}', course_id = '{$_POST['course_id']}'";
            $this->conn->Execute($sqlInsert);
        }

        $_SESSION[SYSTEM_SESSION]['eMsg']="Successfully Added.";
    }

    /**
     * Save Update
     *
     */
    function doSaveEdit(){
        $id = $_GET['edit'];

        $flds = array();
        foreach ($this->Data as $keyData => $valData) {
            $flds[] = "$keyData='$valData'";
        }
        $fields = implode(", ",$flds);

        $sql = "UPDATE db_event_notif.db_student SET $fields WHERE student_id=$id";
        $this->conn->Execute($sql);

        if($_POST['course_id'] != ''){
            $sqlGetCourseInfo = "SELECT dd.*
                                FROM db_event_notif.db_course dc
                                INNER JOIN db_event_notif.db_department dd on dc.department_id = dd.department_id
                                WHERE dc.course_id = '{$_POST['course_id']}'
                                ";
            $rsCI = $this->conn->Execute($sqlGetCourseInfo);
            $sqlInsert = "UPDATE db_event_notif.db_student_rel SET campus_id = '{$rsCI->fields['campus_id']}', department_id = '{$rsCI->fields['department_id']}', course_id = '{$_POST['course_id']}' WHERE student_id = '{$id}'";
            $this->conn->Execute($sqlInsert);
        }

        $_SESSION[SYSTEM_SESSION]['eMsg']="Successfully Updated.";
    }

    /**
     * Delete Record
     *
     * @param string $id_
     */
    function doDelete($id_ = ""){
        $sql = "DELETE FROM db_event_notif.db_student WHERE student_id=?";
        $this->conn->Execute($sql,array($id_));
        $_SESSION[SYSTEM_SESSION]['eMsg']="Successfully Deleted.";
    }

    /**
     * Get all the Table Listings
     *
     * @return array
     */
    function getTableList(){
        // Process the query string and exclude querystring named "p"
        if (!empty($_SERVER['QUERY_STRING'])) {
            $qrystr = explode("&",$_SERVER['QUERY_STRING']);
            foreach ($qrystr as $value) {
                $qstr = explode("=",$value);
                if ($qstr[0]!="p") {
                    $arrQryStr[] = implode("=",$qstr);
                }
            }
            $aQryStr = $arrQryStr;
            $aQryStr[] = "p=@@";
            $queryStr = implode("&",$aQryStr);
        }

        //bby: search module
        $qry = array();
        if (isset($_REQUEST['search_field'])) {

            // lets check if the search field has a value
            if (strlen($_REQUEST['search_field'])>0) {
                // lets assign the request value in a variable
                $search_field = MainBlock::BlockSQLInjection($_REQUEST['search_field']);

                // create a custom criteria in an array
                $qry[] = "(student_gname like '%$search_field%'
                            OR student_mname like '%$search_field%'
                            OR student_lname like '%$search_field%'
                            OR if(is_active = 1, concat('Active'), concat('Not Active')) like '%$search_field%'
                            )";

            }
        }

        // put all query array into one criteria string
        $criteria = (count($qry)>0)?" where ".implode(" and ",$qry):"";

        $viewLink = "";
        $editLink = "<a href=\"?inpage=student&edit=',student_id,'\" class=\"btn btn-sm btn-success\">EDIT";
        $delLink = "<a href=\"?inpage=student&delete=',student_id,'\" onclick=\"return confirm(\'Are you sure, you want to delete?\');\" class=\"btn btn-flat btn-xs bg-orange\"><span class=\"glyphicon glyphicon-trash\"></span> DELETE</a>";

        // Sort field mapping
        $arrSortBy = array(
            "student_gname"=>"student_gname"
        ,"student_mname"=>"student_mname"
        ,"student_lname"=>"student_lname"
        ,"status_ref"=>"if(is_active = 1, concat('Active'), concat('Not Active'))"
        );

        if(isset($_GET['sortby'])){
            $strOrderBy = " order by ".$arrSortBy[$_GET['sortby']]." ".$_GET['sortof'];
        }

        $sql = "SELECT *, if(is_active = 1, concat('Active'), concat('Not Active')) as status_ref, concat('$editLink') as viewdata
                FROM db_event_notif.db_student
                $criteria
                $strOrderBy
                ";

        $sqlcount = "SELECT count(*) as mycount FROM db_event_notif.db_student order by student_id $criteria";

        $arrFields = array(
            "student_gname"=>"Given Name",
            "student_mname"=>"Middle Name",
            "student_lname"=>"Last Name",
            "status_ref"=>"Status",
            "viewdata"=>"&nbsp;"
        );

        $arrAttribs = array(
            "student_gname"=>"style=\"vertical-align: middle;\""
        ,"student_mname"=>"style=\"vertical-align: middle;\""
        ,"status_ref"=>"style=\"vertical-align: middle;\""
        ,"viewdata"=>" style=\"white-space: nowrap;width: 1px\""
        );

        $tblDisplayList = new clsTableList($this->conn);
        $tblDisplayList->arrFields = $arrFields;
        $tblDisplayList->paginator->linkPage = "?$queryStr";
        $tblDisplayList->sqlAll = $sql;
        $tblDisplayList->sqlCount = $sqlcount;

        return $tblDisplayList->getTableList($arrAttribs);
    }

    function updateStatus($id){
        $sql = "SELECT * FROM db_event_notif.db_student WHERE student_id = '{$id}'";
        $rsResult = $this->conn->Execute($sql);
        if(!$rsResult->EOF){
            if($rsResult->fields['is_active'] == 1){
                $flds[] = "is_active = 0";
            }else{
                $flds[] = "is_active = 1";
            }
            $fields = implode(", ",$flds);
            $sqlUpdate = "UPDATE db_event_notif.db_student SET $fields WHERE student_id = '{$id}'";
            $this->conn->Execute($sqlUpdate);
        }
    }

    function getCampusList(){
        $sql = "SELECT *
                FROM db_event_notif.db_campus
                WHERE is_active = 1
                ";
        $rsResult = $this->conn->Execute($sql);
        while(!$rsResult->EOF){
            $arrResult[] = $rsResult->fields;
            $rsResult->MoveNext();
        }
        return $arrResult;
    }

    function getDepartmentList(){
        $sql = "SELECT *
                FROM db_event_notif.db_department
                WHERE is_active = 1
                ";
        $rsResult = $this->conn->Execute($sql);
        while(!$rsResult->EOF){
            $arrResult[] = $rsResult->fields;
            $rsResult->MoveNext();
        }
        return $arrResult;
    }

    function getCourseList(){
        $sql = "SELECT *
                FROM db_event_notif.db_course
                WHERE is_active = 1
                ";
        $rsResult = $this->conn->Execute($sql);
        while(!$rsResult->EOF){
            $arrResult[] = $rsResult->fields;
            $rsResult->MoveNext();
        }
        return $arrResult;
    }

}


?>
