<?php

require_once(SYSCONFIG_CLASS_PATH.'blocks/blockbasephp.class.php');

class MainBlock extends BlockBasePHP {

	var $pageTitle = '';
	var $breadCrumbs = '';
	var $templateTheme = SYSCONFIG_THEME;

	function MainBlock() {
		parent::BlockBasePHP();
		$this->templateDir = SYSCONFIG_THEME_PATH.$this->templateTheme.'/templates/';
		$this->templateFile = 'index_blank_styled.tpl.php';
	}

	function MainBlock_forpopup() {
		parent::BlockBasePHP();
		$this->templateDir = SYSCONFIG_THEME_PATH.$this->templateTheme.'/templates/';
		$this->templateFile = 'index_blank_styled.tpl.php';
	}

	function setBreadCrumbs($bc,$attribs="class='breadCrumbs'") {
		$parts = array();
		foreach ($bc as $bckey => $bclink) {
			if ( $bclink ) {
				$parts[] = "<a href='{$bclink}' {$attribs}>{$bckey}</a>";
			} else {
				$parts[] = "<span {$attribs}>{$bckey}</span>";
			}
		}

		$this->breadCrumbs = implode(' &gt; ',$parts);
	}

    function BlockSQLInjection($pData){

        $reference = array(
            array('ref'=>'"', 'value'=>'\"')
        , array('ref'=>"'", 'value'=>"\'")
        , array('ref'=>"‘", 'value'=>"&lsquo;")
        , array('ref'=>"’", 'value'=>"&rsquo;")
        , array('ref'=>"“", 'value'=>"&ldquo;")
        , array('ref'=>"”", 'value'=>"&rdquo;")
        , array('ref'=>"′", 'value'=>"&prime;")
        , array('ref'=>"‵", 'value'=>"&bprime;")
        , array('ref'=>"`", 'value'=>"&grave;")
        , array('ref'=>"´", 'value'=>"&acute;")
//        , array('ref'=>"&", 'value'=>"&percnt;")
//        , array('ref'=>"!", 'value'=>"&excl;")
//        , array('ref'=>"(", 'value'=>"&lpar;")
//        , array('ref'=>")", 'value'=>"&rpar;")
//        , array('ref'=>"*", 'value'=>"&ast;")
//        , array('ref'=>"+", 'value'=>"&plus;")
//        , array('ref'=>",", 'value'=>"&comma;")
//        , array('ref'=>".", 'value'=>"&period;")
//        , array('ref'=>"/", 'value'=>"&sol;")
//        , array('ref'=>":", 'value'=>"&colon;")
//        , array('ref'=>";", 'value'=>"&semi;")
//        , array('ref'=>"<", 'value'=>"&lt;")
//        , array('ref'=>">", 'value'=>"&gt;")
        , array('ref'=>"=", 'value'=>"&equals;")
        , array('ref'=>"?", 'value'=>"&quest;")
//        , array('ref'=>"@", 'value'=>"&commat;")
//        , array('ref'=>"[", 'value'=>"&lsqb;")
//        , array('ref'=>"]", 'value'=>"&rsqb;")
//        , array('ref'=>"{", 'value'=>"&lcub;")
//        , array('ref'=>"}", 'value'=>"&rcub;")
        , array('ref'=>"|", 'value'=>"&verbar;")
        , array('ref'=>"Ñ", 'value'=>"&Ntilde;")
        , array('ref'=>"ñ", 'value'=>"&ntilde;")
        );
        $search = array_column($reference, 'ref');
        $replace = array_column($reference, 'value');

        if(is_array($pData)){
            foreach($pData as $key=>$value){
                if(!is_numeric($value)){
                    $returnValue[$key] = str_replace($search, $replace, $value);
                }elseif(is_array($value)){
                    $returnValue[$key] = MainBlock::BlockSQLInjection($value);
                }else{
                    $returnValue[$key] = str_replace($search, $replace, $value);
                }
            }
        }else{
            if(is_string($pData)){
                $returnValue = str_replace($search, $replace, $pData);
            }
        }

        return $returnValue;
    }

    function getDefaultSiteSettings($dbconn_){
        $_SESSION['buena_session']['site_setup']['site_title'] = 'BUENS SYSTEM';
        $_SESSION['buena_session']['site_setup']['site_desc'] = '';
        $_SESSION['buena_session']['site_setup']['site_logo'] = '../components/images/empty_logo_square.png';

        $sql = "SELECT *
                FROM db_event_notif.db_site
                WHERE is_active = 1
                ";
        $rsResult = $dbconn_->Execute($sql);
        if(!$rsResult->EOF){
            $_SESSION['buena_session']['site_setup']['site_title'] = $rsResult->fields['site_title'];
            $_SESSION['buena_session']['site_setup']['site_desc'] = $rsResult->fields['site_desc'];
            $_SESSION['buena_session']['site_setup']['site_logo'] = 'setup.php?inpage=site&image='.$rsResult->fields['site_id'];
        }
    }

    function getSliders($dbconn_){
        $sql = "SELECT si_id
                FROM db_event_notif.db_slider_img
                WHERE is_active = 1
                ORDER BY si_ordinal ASC, si_name ASC
                ";
        $rsResult = $dbconn_->Execute($sql);
        while(!$rsResult->EOF){
            $arrResult[] = $rsResult->fields;
            $rsResult->MoveNext();
        }
        return $arrResult;
    }


    function getUserOutOfPage(){
        if(count($_SESSION[SYSTEM_SESSION]['user_id']) <= 0){
            header("location: admin.php?inpage=logout");
            exit();
        }
    }

}
?>