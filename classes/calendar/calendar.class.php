<?php

class clsCalendar{

    var $conn;
    var $fieldMap;
    var $Data;

    /**ss
     * Class Constructor
     *
     * @param object $dbconn_
     * @return clsCalendar object
     */
    function clsCalendar($dbconn_ = null){
        $this->conn =& $dbconn_;
        $this->fieldMap = array(
            "student_gname" => "student_gname",
            "student_mname" => "student_mname",
            "student_lname" => "student_lname",
            "student_address" => "student_address",
            "student_mobile" => "student_mobile"
        );
    }

    function getEvents(){
        $sql = "SELECT *
                FROM db_event_notif.db_event
                WHERE is_active = 1
                ";
        $rsResult = $this->conn->Execute($sql);
        while(!$rsResult->EOF){
            $arrResult[] = $rsResult->fields;
            $rsResult->MoveNext();
        }
        return $arrResult;
    }

    function setToEvents($arr = array()){
//        events: [{
//                title: 'All Day Event',
//                    start: new Date(y, m, d)
//                }, {
//                title: 'Long asd',
//                    editable: true,
//                    start: new Date(y, m, d - 5),
//                    end: new Date(y, m, d - 2)
//                }, {
//                title: 'Meeting',
//                    start: new Date(y, m, d, 10, 30),
//                    allDay: false
//                }, {
//                title: 'Lunch',
//                    start: new Date(y, m, d + 14, 12, 0),
//                    end: new Date(y, m, d, 14, 0),
//                    allDay: false
//                }, {
//                title: 'Birthday Party',
//                    start: new Date(y, m, d + 1, 19, 0),
//                    end: new Date(y, m, d + 1, 22, 30),
//                    allDay: false
//                }, {
//                title: 'Click for Google',
//                    start: new Date(y, m, 28),
//                    end: new Date(y, m, 29),
//                    url: 'http://google.com/'
//                }]

        $toReturnValue = "";
        $toReturnValue .= "events: [";

        $dateTimeNow = date("Y-m-d H:i:s A");

        if(count($arr) > 0){
            foreach($arr as $key=>$value){
                $startDate = date("Y-m-d", strtotime($value['event_start_datetime']));
                $startTime = date("H:i:s", strtotime($value['event_start_datetime']));
                $endDate = date("Y-m-d", strtotime($value['event_end_datetime']));
                $endTime = date("H:i:s", strtotime($value['event_end_datetime']));

                if(explode(' ', $startTime)[0] == '00:00:00' && explode(' ', $endTime)[0] == '00:00:00'){
                    $allDay = 'true';
                    $start = $startDate." 00:00:00";
                    $end = $endDate." 24:00:00";
                }else{
                    $allDay = null;
                    $start = $startDate." ".$startTime;
                    $end = $endDate." ".$endTime;
                }

                if($allDay != null){
                    $arrTemp[] = "allDay: ".$allDay;
                }
                if($start != null){
                    $arrTemp[] = "start: new Date('".$start."')";
                }
                if($end != null){
                    $arrTemp[] = "end: new Date('".$end."')";
                }

                $arrTempImploded = count($arrTemp) > 0 ? ", " . implode(', ', $arrTemp) : "";

                $toReturnValueArrray[] = "{ title: '".$value['event_title']."', desc: '".$value['event_desc']."', event_id: '".$value['event_id']."'".$arrTempImploded." }";

                unset($arrTemp);
            }
        }

        $toReturnValueArrrayImploded = implode(", ", $toReturnValueArrray);
        $toReturnValue .= $toReturnValueArrrayImploded;
        unset($toReturnValueArrray);

        $toReturnValue .= "]";

        return $toReturnValue;
    }

    function doSaveEdit($pData = array()){
        $pData['event_title'] = addslashes($pData['event_title']);
        $pData['event_desc'] = addslashes($pData['event_desc']);
        $flds[] = "event_title = '{$pData['event_title']}'";
        $flds[] = "event_desc = '{$pData['event_desc']}'";
        $event_start_datetime = $pData['event_start_datetime_date'].($pData['event_start_datetime_time'] == '' ? " 00:00:00" : " ".date("H:i:s", strtotime($pData['event_start_datetime_time'])));
        $event_end_datetime = $pData['event_end_datetime_date'].($pData['event_end_datetime_time'] == '' ? " 00:00:00" : " ".date("H:i:s", strtotime($pData['event_end_datetime_time'])));
        $flds[] = "event_start_datetime = '{$event_start_datetime}'";
        $flds[] = "event_end_datetime = '{$event_end_datetime}'";
        $flds[] = "participants_ids = '{$pData['participant_ids']}'";
        $fields = implode(", ", $flds);

        if($pData['event_id'] > 0){
            $qry[] = "event_id = '{$pData['event_id']}'";
            $criteria = (count($qry) > 0) ? "WHERE " . implode(" AND ", $qry) : "";

            $sql = "UPDATE db_event_notif.db_event SET $fields $criteria";
            $event = "Updated";
            $eventPlural = "Update";
        }else{
            $sql = "INSERT INTO db_event_notif.db_event SET $fields";
            $event = "Created";
            $eventPlural = "Create";
        }

        $rsResult = $this->conn->Execute($sql);

        if($rsResult){
            $_SESSION[SYSTEM_SESSION]['eMsg'] = "Event Successfully {$event}";
        }else{
            $_SESSION[SYSTEM_SESSION]['eMsg'][] = "Event Failed to {$eventPlural}";
        }

    }

    function createUnivSql(){
        $sqls[] = "SELECT concat(campus_id) as ids, concat(campus_shortname) as nameList
                    FROM db_event_notif.db_campus
                    WHERE is_active = 1
                    ";
        $sqls[] = "SELECT concat(dc.campus_id, '-', dd.department_id) as ids, concat(dc.campus_shortname,' ( ', department_shortname, ' )') as nameList
                    FROM db_event_notif.db_campus dc
                    INNER JOIN db_event_notif.db_department dd on dc.campus_id = dd.campus_id
                    WHERE dc.is_active = 1
                    AND dd.is_active = 1
                    ";
        $sqls[] = "SELECT concat(dc.campus_id, '-', dd.department_id, '-', dco.course_id) as ids, concat(dc.campus_shortname,' ( ', dd.department_shortname, ' - ', dco.course_shortname, ' )') as nameList
                    FROM db_event_notif.db_campus dc
                    INNER JOIN db_event_notif.db_department dd on dc.campus_id = dd.campus_id
                    INNER JOIN db_event_notif.db_course dco on dd.department_id = dco.department_id
                    WHERE dc.is_active = 1
                    AND dd.is_active = 1
                    AND dco.is_active = 1
                    ";
        $sqls[] = "SELECT concat(dc.campus_id, '-', dd.department_id, '-', dco.course_id, '-', st.student_id) as ids, concat(dc.campus_shortname,' ( ', dd.department_shortname, ' - ', dco.course_shortname, ' ) ', UCASE(st.student_lname), ', ', UCASE(st.student_gname)) as nameList
                    FROM db_event_notif.db_campus dc
                    INNER JOIN db_event_notif.db_department dd on dc.campus_id = dd.campus_id
                    INNER JOIN db_event_notif.db_course dco on dd.department_id = dco.department_id
                    INNER JOIN db_event_notif.db_student_rel sr on (dc.campus_id = sr.campus_id AND dd.department_id = sr.department_id AND dco.course_id = sr.course_id)
                    INNER JOIN db_event_notif.db_student st on sr.student_id = st.student_id
                    WHERE dc.is_active = 1
                    AND dd.is_active = 1
                    AND dco.is_active = 1
                    ";
        $sql = implode(" UNION ALL ", $sqls) . " ORDER BY nameList ASC";

        return $sql;
    }

    function participantSelect(){
        $rsResult = $this->conn->Execute($this->createUnivSql());
        while(!$rsResult->EOF){
            $returnValueTemp[] = "{ id: '".$rsResult->fields['ids']."', text: '".$rsResult->fields['nameList']."' }";
            $rsResult->MoveNext();
        }

        $returnValue = implode(", ", $returnValueTemp);

        return $returnValue;
    }

    function getEventInfo($event_id){
        $sql = "SELECT *
                FROM db_event_notif.db_event
                WHERE event_id = '{$event_id}'
                ";
        $rsResult = $this->conn->Execute($sql);
        if(!$rsResult->EOF){
            $participants = explode(',', $rsResult->fields['participants_ids']);
            $paticipants_modified[] = $participants;
            return $paticipants_modified;
        }
    }

    function getEventView($event_id){
        $sql = "SELECT *
                FROM db_event_notif.db_event
                WHERE event_id = '{$event_id}'
                ";
        $rsResult = $this->conn->Execute($sql);
        if(!$rsResult->EOF){

            $rsResult2 = $this->conn->Execute($this->createUnivSql());
            while(!$rsResult2->EOF){
                $arrResult[$rsResult2->fields['ids']] = $rsResult2->fields;
                $rsResult2->MoveNext();
            }

            $explodeIds = explode(",", $rsResult->fields['participants_ids']);

            foreach($explodeIds as $key=>$value){
                $returnValue[] = $arrResult[$value];
            }

            $nameList = array_column($returnValue, 'nameList');

            $returnValue = (count($nameList) > 0) ? "* " . implode("\n* ", $nameList) : "";

            return $returnValue;
        }
    }

}


?>
