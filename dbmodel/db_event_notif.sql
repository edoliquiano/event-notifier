-- MySQL dump 10.14  Distrib 5.5.56-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: db_event_notif
-- ------------------------------------------------------
-- Server version	5.5.56-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `db_event_notif`
--

/*!40000 DROP DATABASE IF EXISTS `db_event_notif`*/;

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `db_event_notif` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `db_event_notif`;

--
-- Table structure for table `db_campus`
--

DROP TABLE IF EXISTS `db_campus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_campus` (
  `campus_id` int(11) NOT NULL AUTO_INCREMENT,
  `campus_name` varchar(200) DEFAULT NULL,
  `campus_shortname` varchar(200) DEFAULT NULL,
  `campus_desc` varchar(200) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1' COMMENT '1 = active, 0 = not active',
  PRIMARY KEY (`campus_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `db_campus`
--

LOCK TABLES `db_campus` WRITE;
/*!40000 ALTER TABLE `db_campus` DISABLE KEYS */;
/*!40000 ALTER TABLE `db_campus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `db_course`
--

DROP TABLE IF EXISTS `db_course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_course` (
  `course_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_name` varchar(200) DEFAULT NULL,
  `course_shortname` varchar(200) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1' COMMENT '1 = active, 0 = not active',
  `department_id` int(11) NOT NULL,
  PRIMARY KEY (`course_id`),
  KEY `department_id` (`department_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `db_course`
--

LOCK TABLES `db_course` WRITE;
/*!40000 ALTER TABLE `db_course` DISABLE KEYS */;
/*!40000 ALTER TABLE `db_course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `db_department`
--

DROP TABLE IF EXISTS `db_department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_department` (
  `department_id` int(11) NOT NULL AUTO_INCREMENT,
  `department_name` varchar(200) DEFAULT NULL,
  `department_shortname` varchar(200) DEFAULT NULL,
  `department_desc` varchar(200) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1' COMMENT '1 = active, 0 = not active',
  `campus_id` int(11) NOT NULL,
  PRIMARY KEY (`department_id`),
  KEY `campus_id` (`campus_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `db_department`
--

LOCK TABLES `db_department` WRITE;
/*!40000 ALTER TABLE `db_department` DISABLE KEYS */;
/*!40000 ALTER TABLE `db_department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `db_developer`
--

DROP TABLE IF EXISTS `db_developer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_developer` (
  `dev_id` int(11) NOT NULL AUTO_INCREMENT,
  `dev_gname` varchar(255) NOT NULL,
  `dev_lname` varchar(255) NOT NULL,
  `dev_mname` varchar(255) NOT NULL,
  `dev_address` text NOT NULL,
  `dev_desc` text NOT NULL,
  PRIMARY KEY (`dev_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `db_developer`
--

LOCK TABLES `db_developer` WRITE;
/*!40000 ALTER TABLE `db_developer` DISABLE KEYS */;
/*!40000 ALTER TABLE `db_developer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `db_event`
--

DROP TABLE IF EXISTS `db_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_event` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_title` varchar(255) NOT NULL,
  `event_desc` text NOT NULL,
  `event_start_datetime` datetime NOT NULL,
  `event_end_datetime` datetime NOT NULL,
  `participants_ids` varchar(500) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1' COMMENT '1 = active, 0 = not active',
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `db_event`
--

LOCK TABLES `db_event` WRITE;
/*!40000 ALTER TABLE `db_event` DISABLE KEYS */;
/*!40000 ALTER TABLE `db_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `db_message`
--

DROP TABLE IF EXISTS `db_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_message` (
  `dm_id` int(11) NOT NULL AUTO_INCREMENT,
  `dm_number` varchar(13) NOT NULL,
  `dm_content` text NOT NULL,
  `dm_addwhen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dm_addwho` varchar(100) NOT NULL,
  `student_id` int(11) NOT NULL,
  PRIMARY KEY (`dm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `db_message`
--

LOCK TABLES `db_message` WRITE;
/*!40000 ALTER TABLE `db_message` DISABLE KEYS */;
/*!40000 ALTER TABLE `db_message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `db_modules`
--

DROP TABLE IF EXISTS `db_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_modules` (
  `module_id` int(11) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(200) DEFAULT NULL,
  `module_desc` varchar(200) DEFAULT NULL,
  `module_icon` varchar(200) DEFAULT NULL,
  `module_parent` int(11) NOT NULL DEFAULT '0',
  `module_ord` int(11) DEFAULT NULL,
  `module_link` varchar(200) DEFAULT NULL,
  `module_display_side` int(11) DEFAULT '1',
  `module_status` int(11) NOT NULL DEFAULT '1' COMMENT '1 = active, 0 = not active',
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `db_modules`
--

LOCK TABLES `db_modules` WRITE;
/*!40000 ALTER TABLE `db_modules` DISABLE KEYS */;
INSERT INTO `db_modules` VALUES (1,'Home','Home','fa-home',0,10,'../index.php',1,1),(2,'Setup','Setup','fa-cog',0,20,'',1,1),(3,'Campus Setup','Campus Setup','',2,10,'setup.php?inpage=campus',1,1),(4,'Department Setup','Department Setup','',2,20,'setup.php?inpage=department',1,1),(5,'Course Setup','Course Setup','',2,30,'setup.php?inpage=course',1,1),(6,'Student List','Student List','fa-group',0,30,'student.php',1,1),(7,'Site Setup','Site Setup','',2,5,'setup.php?inpage=site',1,1),(8,'Site Slider Setup','Site Slider Setup','',2,6,'setup.php?inpage=slider',1,1);
/*!40000 ALTER TABLE `db_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `db_schoolyear`
--

DROP TABLE IF EXISTS `db_schoolyear`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_schoolyear` (
  `sy_id` int(11) NOT NULL AUTO_INCREMENT,
  `sy_from` varchar(100) DEFAULT NULL,
  `sy_to` varchar(100) DEFAULT NULL,
  `sy_name` varchar(100) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1' COMMENT '1 = active, 0 = not active',
  PRIMARY KEY (`sy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `db_schoolyear`
--

LOCK TABLES `db_schoolyear` WRITE;
/*!40000 ALTER TABLE `db_schoolyear` DISABLE KEYS */;
/*!40000 ALTER TABLE `db_schoolyear` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `db_site`
--

DROP TABLE IF EXISTS `db_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_site` (
  `site_id` int(11) NOT NULL AUTO_INCREMENT,
  `site_title` varchar(255) NOT NULL,
  `site_desc` text NOT NULL,
  `site_logo` mediumblob,
  `is_active` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `db_site`
--

LOCK TABLES `db_site` WRITE;
/*!40000 ALTER TABLE `db_site` DISABLE KEYS */;
/*!40000 ALTER TABLE `db_site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `db_slider_img`
--

DROP TABLE IF EXISTS `db_slider_img`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_slider_img` (
  `si_id` int(11) NOT NULL AUTO_INCREMENT,
  `si_name` varchar(255) DEFAULT NULL,
  `si_desc` text,
  `si_img` mediumblob,
  `si_ordinal` int(11) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`si_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `db_slider_img`
--

LOCK TABLES `db_slider_img` WRITE;
/*!40000 ALTER TABLE `db_slider_img` DISABLE KEYS */;
/*!40000 ALTER TABLE `db_slider_img` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `db_student`
--

DROP TABLE IF EXISTS `db_student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_student` (
  `student_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_gname` varchar(200) DEFAULT NULL,
  `student_mname` varchar(200) DEFAULT NULL,
  `student_lname` varchar(200) DEFAULT NULL,
  `student_address` varchar(200) DEFAULT NULL,
  `student_mobile` varchar(13) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1' COMMENT '1 = active, 0 = not active',
  PRIMARY KEY (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `db_student`
--

LOCK TABLES `db_student` WRITE;
/*!40000 ALTER TABLE `db_student` DISABLE KEYS */;
/*!40000 ALTER TABLE `db_student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `db_student_rel`
--

DROP TABLE IF EXISTS `db_student_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_student_rel` (
  `student_id` int(11) DEFAULT NULL,
  `campus_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `db_student_rel`
--

LOCK TABLES `db_student_rel` WRITE;
/*!40000 ALTER TABLE `db_student_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `db_student_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `db_user`
--

DROP TABLE IF EXISTS `db_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(500) DEFAULT NULL,
  `user_pass_raw` varchar(500) DEFAULT NULL,
  `user_pass_md5` varchar(500) DEFAULT NULL,
  `usertype_id` int(11) NOT NULL DEFAULT '1',
  `is_active` int(11) NOT NULL DEFAULT '1' COMMENT '1 = active, 0 = not active',
  PRIMARY KEY (`user_id`),
  KEY `usertype_id` (`usertype_id`),
  CONSTRAINT `db_user_ibfk_1` FOREIGN KEY (`usertype_id`) REFERENCES `db_usertype` (`usertype_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `db_user`
--

LOCK TABLES `db_user` WRITE;
/*!40000 ALTER TABLE `db_user` DISABLE KEYS */;
INSERT INTO `db_user` VALUES (1,'admin','admin','21232f297a57a5a743894a0e4a801fc3',1,1);
/*!40000 ALTER TABLE `db_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `db_usertype`
--

DROP TABLE IF EXISTS `db_usertype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_usertype` (
  `usertype_id` int(11) NOT NULL AUTO_INCREMENT,
  `usertype_name` varchar(200) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1' COMMENT '1 = active, 0 = not active',
  PRIMARY KEY (`usertype_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `db_usertype`
--

LOCK TABLES `db_usertype` WRITE;
/*!40000 ALTER TABLE `db_usertype` DISABLE KEYS */;
INSERT INTO `db_usertype` VALUES (1,'Super Administrator',1),(2,'Administrator',1);
/*!40000 ALTER TABLE `db_usertype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `db_usertype_access`
--

DROP TABLE IF EXISTS `db_usertype_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_usertype_access` (
  `access_id` int(11) NOT NULL AUTO_INCREMENT,
  `usertype_id` int(11) DEFAULT NULL,
  `usertype_name` varchar(200) DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`access_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `db_usertype_access`
--

LOCK TABLES `db_usertype_access` WRITE;
/*!40000 ALTER TABLE `db_usertype_access` DISABLE KEYS */;
INSERT INTO `db_usertype_access` VALUES (1,1,'Super Administrator',1),(2,1,'Super Administrator',2),(3,1,'Super Administrator',3),(4,1,'Super Administrator',4),(5,1,'Super Administrator',5),(6,1,'Super Administrator',6),(7,1,'Super Administrator',7),(8,1,'Super Administrator',8);
/*!40000 ALTER TABLE `db_usertype_access` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-29  7:15:41
