<?php
require_once(SYSCONFIG_CLASS_PATH.'blocks/mainblock.class.php');
require_once(SYSCONFIG_CLASS_PATH.'admin/application.class.php');
require_once(SYSCONFIG_CLASS_PATH.'util/tablelist.class.php');
require_once(SYSCONFIG_CLASS_PATH.'calendar/calendar.class.php');

Application::app_initialize();

$dbconn = Application::db_open();
//$dbconn->debug = true;

$cmap = array(
    "default" => "calendar.tpl.php",
    "add" => "student_form.tpl.php",
    "edit" => "student_form.tpl.php"
);

$cmapKey = (isset($_GET['action']))?$_GET['action']:"default";
$arrbreadCrumbs = array(
    'Calendar' => ''
);

$cmapKey = isset($_GET['edit'])?"edit":$cmapKey;
$cmapKey = isset($_GET['delete'])?"delete":$cmapKey;

// Instantiate the MainBlock Class
$mainBlock = new MainBlock();
$mainBlock->assign('PageTitle','Student List');
$mainBlock->templateDir .= "admin";
if($_SESSION[SYSTEM_SESSION]['user_id'] > 0){
    $mainBlock->templateFile = "../index_sidebar_header.tpl.php";
}else{
    $mainBlock->templateFile = "../index_header.tpl.php";
}


// Instantiate the BlockBasePHP for Center Panel Display
$centerPanelBlock = new BlockBasePHP();
$centerPanelBlock->templateDir  .= "calendar";
$centerPanelBlock->templateFile = $cmap[$cmapKey];

/*-!-!-!-!-!-!-!-!-*/

$objClsCalendar = new clsCalendar($dbconn);

switch ($cmapKey) {

    default:
//        $arrbreadCrumbs['Student List'] = "";
        $events = $objClsCalendar->getEvents();
        $mainBlock->assign('participantList', $objClsCalendar->participantSelect());
        $mainBlock->assign('eventTableList', $objClsCalendar->setToEvents($events));

        if($_GET['ajaxTrigger'] == 1){
            if($_SESSION[SYSTEM_SESSION]['user_id'] > 0){
                die(json_encode($objClsCalendar->getEventInfo($_GET['event_id'])));
            }else{
                die(json_encode($objClsCalendar->getEventView($_GET['event_id'])));
            }
        }

        if(count($_POST) > 0){ $_POST = MainBlock::BlockSQLInjection($_POST);
            $objClsCalendar->doSaveEdit($_POST);
            header("location: calendar.php");
            exit();
        }

        break;
}

if(isset($_SESSION[SYSTEM_SESSION]['eMsg'])){
    $mainBlock->assign('eMsg',$_SESSION[SYSTEM_SESSION]['eMsg']);
    unset($_SESSION[SYSTEM_SESSION]['eMsg']);
}

/*-!-!-!-!-!-!-!-!-*/

$mainBlock->assign('centerPanel',$centerPanelBlock);
$mainBlock->setBreadCrumbs($arrbreadCrumbs);
$mainBlock->assign('breadCrumbs',$mainBlock->breadCrumbs);
$mainBlock->displayBlock();


?>
