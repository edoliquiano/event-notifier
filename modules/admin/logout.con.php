<?php
include_once("../configurations/common.config.php");
require_once(SYSCONFIG_CLASS_PATH.'blocks/mainblock.class.php');
require_once(SYSCONFIG_CLASS_PATH.'admin/application.class.php');

Application::app_initialize();

$dbconn = Application::db_open();

$mainBlock = new MainBlock();
session_start();

//session_destroy();
unset($_SESSION[SYSTEM_SESSION]);
header("Location: ../index.php");
exit;
?>