<?php
include_once(SYSCONFIG_CLASS_PATH."admin/application.class.php");
include_once(SYSCONFIG_CLASS_PATH."admin/appauth.class.php");
include_once(SYSCONFIG_CLASS_PATH."blocks/mainblock.class.php");
include_once(SYSCONFIG_CLASS_PATH."util/ddate.class.php");
include_once(SYSCONFIG_CLASS_PATH."calendar/calendar.class.php");

//Application::app_initialize(array("login_required"=> true));
Application::app_initialize();
session_start();


$dbconn = Application::db_open();
//$dbconn->debug = true;

$mainBlock = new MainBlock();

$authResult = true;

if(count($_POST)>0){ $_POST = MainBlock::BlockSQLInjection($_POST);
    $userAuth = new AppAuth($dbconn);
    $authResult = $userAuth->doAuth($_POST['user_name'],md5($_POST['user_password']));
    if($authResult){
        $_SESSION[SYSTEM_SESSION]['user_id'] = $userAuth->userID;
        $_SESSION[SYSTEM_SESSION]['user_name'] = $userAuth->userName;
        $_SESSION[SYSTEM_SESSION]['usertype_name'] = $userAuth->userType;
        $_SESSION[SYSTEM_SESSION]['user_last_login'] = date("M d, Y H:i:s A");
        $_SESSION[SYSTEM_SESSION]['user_data'] = $userAuth->Data;

        $mnuInfo = array(
            "default" => array(
                "info" => "SELECT * FROM db_event_notif.db_modules am
                            INNER JOIN db_event_notif.db_usertype_access aut on am.module_id = aut.module_id
                            INNER JOIN db_event_notif.db_user au on aut.usertype_id = au.usertype_id
                            WHERE aut.usertype_name = ?
                            AND au.user_id=?
                            AND am.module_parent = 0
                            AND am.module_display_side = 1
                            ORDER BY module_ord ASC",
                "module" => "SELECT * FROM db_event_notif.db_modules appmod
                            INNER JOIN db_event_notif.db_usertype_access auta on appmod.module_id = auta.module_id
                            INNER JOIN db_event_notif.db_user au on au.usertype_id = auta.usertype_id
                            WHERE appmod.module_status=1
                            AND appmod.module_parent=?
                            AND auta.usertype_name=?
                            AND au.user_id=?
                            AND appmod.module_display_side = 1
                            ORDER BY appmod.module_ord ASC"
            )
        );

        $mnuInfoTop = array(
            "default" => array(
                "info" => "SELECT * FROM db_event_notif.db_modules am
                            INNER JOIN db_event_notif.db_usertype_access aut on am.module_id = aut.module_id
                            INNER JOIN db_event_notif.db_user au on aut.usertype_id = au.usertype_id
                            WHERE aut.usertype_name = ?
                            AND au.user_id=?
                            AND am.module_parent = 0
                            AND am.module_display_side = 0
                            ORDER BY module_ord ASC",
                "module" => "SELECT * FROM db_event_notif.db_modules appmod
                            INNER JOIN db_event_notif.db_usertype_access auta on appmod.module_id = auta.module_id
                            INNER JOIN db_event_notif.db_user au on au.usertype_id = auta.usertype_id
                            WHERE appmod.module_status=1
                            AND appmod.module_parent=?
                            AND auta.usertype_name=?
                            AND au.user_id=?
                            AND appmod.module_display_side = 0
                            ORDER BY appmod.module_ord ASC"
            )
        );

        foreach ($mnuInfo as $miKey => $miValue) {
            $mnuScript = $mnuArr = "";
            $arrMainMenu = $userAuth->getMenuModules($miValue['info'], $dbconn);
            if (count($arrMainMenu) > 0) {
                $mnuArr2 = $userAuth->getModules2($dbconn, $arrMainMenu, false, 0, $miValue['module']);
            }

            $mnuScript .= '<ul class="nav side-menu">';
//            $mnuScript .= '<li class="header"><hr></li>';
            $mnuScript .= $mnuArr2;
            $mnuScript .= '</ul>';

            if(!empty($_SESSION[SYSTEM_SESSION]['user_data'])){
                $_SESSION[SYSTEM_SESSION]['menuinfo'][$miKey] = $mnuScript;
            }
            unset($mnuArr2);
        }

        $_SESSION[SYSTEM_SESSION]['user_menu'] = $_SESSION[SYSTEM_SESSION]['menuinfo']['default'];

        foreach ($mnuInfoTop as $miKey => $miValue) {
            $mnuScriptTop = $mnuArrTop = "";
            $arrMainMenuTop = $userAuth->getMenuModules($miValue['info'], $dbconn);
            if (count($arrMainMenuTop) > 0) {
                $mnuArrTop = $userAuth->getModulesTop($dbconn, $arrMainMenuTop, false, 0, $miValue['module']);
            }

            $mnuScriptTop .= '<ul class="nav navbar-nav navbar-left">';
//            $mnuScriptTop .= '<li class="header"><hr></li>';
            $mnuScriptTop .= $mnuArrTop;
            $mnuScriptTop .= '</ul>';

            if(!empty($_SESSION[SYSTEM_SESSION]['user_data'])){
                $_SESSION[SYSTEM_SESSION]['menuinfotop'][$miKey] = $mnuScriptTop;
            }
            unset($mnuArrTop);
        }

        $_SESSION[SYSTEM_SESSION]['user_menu_top'] = $_SESSION[SYSTEM_SESSION]['menuinfotop']['default'];

        header("Location: ../index.php");
        exit;
    }else{
        $authResult = false;
    }
}

$mainBlock->templateDir .= "admin";
if(count($_SESSION['buena_session']['site_setup']) <= 0){
    $mainBlock->getDefaultSiteSettings($dbconn);
    header("location: admin.php");
}
if(!isset($_SESSION[SYSTEM_SESSION]['user_id'])){
//    $mainBlock->templateFile = "../index_blank_styled.tpl.php";
    $centerPanelBlock = new BlockBasePHP();
    $centerPanelBlock->templateDir .= "admin";

    $centerPanelBlock->assign('sliderList', $mainBlock->getSliders($dbconn));

    if(isset($_GET['login'])){
        $mainBlock->templateFile = "../index_header.tpl.php";
        $centerPanelBlock->templateFile = "login.tpl.php";
    }else{
        $mainBlock->templateFile = "../index_enno.tpl.php";
        $centerPanelBlock->templateFile = "homepage.tpl.php";
    }

    if(!$authResult){
        $centerPanelBlock->assign("errMsg","Invalid username and password!");
    }
    $mainBlock->assign("centerPanel",$centerPanelBlock);
    $mainBlock->displayBlock();

}else{
    header("location: calendar.php");


//    $mainBlock->templateFile = "../index_sidebar_header.tpl.php";
//
//    $objClsCalendar = new clsCalendar($dbconn);
//    $events = $objClsCalendar->getEvents();
//    $mainBlock->assign('eventTableList', $objClsCalendar->setToEvents($events));
//
//    if(count($_POST) > 0){ $_POST = MainBlock::BlockSQLInjection($_POST);
//        $objClsCalendar->doSaveEdit($_POST);
//        header("location: admin.php");
//        exit();
//    }
//
//    $centerPanelBlock = new BlockBasePHP();
//    $centerPanelBlock->templateDir .= "calendar";
//    $centerPanelBlock->templateFile = "calendar.tpl.php";
//
////    $mainBlock->assign('indexErrMsg',$indexErrMsg);
//    $mainBlock->assign("centerPanel",$centerPanelBlock);
//
//    $mainBlock->displayBlock();
}

?>