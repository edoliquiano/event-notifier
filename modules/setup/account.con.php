<?php
require_once(SYSCONFIG_CLASS_PATH.'blocks/mainblock.class.php');
require_once(SYSCONFIG_CLASS_PATH.'admin/application.class.php');
require_once(SYSCONFIG_CLASS_PATH.'util/tablelist.class.php');
require_once(SYSCONFIG_CLASS_PATH.'setup/account.class.php');

Application::app_initialize(array("login_required"=> true));

$dbconn = Application::db_open();
//$dbconn->debug = true;

$cmap = array(
    "default" => "account.tpl.php",
    "add" => "account_form.tpl.php",
    "edit" => "account_form.tpl.php"
);

$cmapKey = (isset($_GET['action']))?$_GET['action']:"default";
$arrbreadCrumbs = array(
    'Setup' => ''
);

$cmapKey = isset($_GET['edit'])?"edit":$cmapKey;
$cmapKey = isset($_GET['delete'])?"delete":$cmapKey;

// Instantiate the MainBlock Class
$mainBlock = new MainBlock();
$mainBlock->assign('PageTitle','Account Setup');
$mainBlock->templateDir .= "admin";
$mainBlock->templateFile = "../index_sidebar_header.tpl.php";


// Instantiate the BlockBasePHP for Center Panel Display
$centerPanelBlock = new BlockBasePHP();
$centerPanelBlock->templateDir  .= "setup";
$centerPanelBlock->templateFile = $cmap[$cmapKey];

/*-!-!-!-!-!-!-!-!-*/

$objClsAccountSetup = new clsAccountSetup($dbconn);

switch ($cmapKey) {
    case 'add':
        $arrbreadCrumbs['Account'] = '';
        $arrbreadCrumbs['Add New'] = "";
        if (count($_POST)>0) { $_POST = MainBlock::BlockSQLInjection($_POST);
            // save add new
            if(!$objClsAccountSetup->doValidateData($_POST)){
                $objClsAccountSetup->doPopulateData($_POST);
                $centerPanelBlock->assign("oData",$objClsAccountSetup->Data);
            }else {
                $objClsAccountSetup->doPopulateData($_POST);
                $objClsAccountSetup->doSaveAdd();
                header("Location: setup.php?inpage=account");
                exit;
            }
        }
        break;

    case 'edit':
        $arrbreadCrumbs['Account'] = '';
        $arrbreadCrumbs['Edit'] = "";

        if (count($_POST)>0) { $_POST = MainBlock::BlockSQLInjection($_POST);
            // update
            if(!$objClsAccountSetup->doValidateData($_POST)){
                $objClsAccountSetup->doPopulateData($_POST);
                $centerPanelBlock->assign("oData",$objClsAccountSetup->Data);
            }else {
                $objClsAccountSetup->doPopulateData($_POST);
                $objClsAccountSetup->doSaveEdit();
                header("Location: admin.php");
                exit;
            }
        }else{
            $oData = $objClsAccountSetup->dbFetch($_GET['edit']);
            $centerPanelBlock->assign("oData",$oData);
        }
        break;

    case "update_status":
        $objClsAccountSetup->updateStatus($_GET['id']);
        exit();
        break;

    case "delete":
        $objClsAccountSetup->doDelete($_GET['delete']);
        header("Location: setup.php?inpage=account");
        exit;
        break;

    default:
        $arrbreadCrumbs['Account'] = "";
        $centerPanelBlock->assign('tblDataList',$objClsAccountSetup->getTableList());
        break;
}

if(isset($_SESSION[SYSTEM_SESSION]['eMsg'])){
    $mainBlock->assign('eMsg',$_SESSION[SYSTEM_SESSION]['eMsg']);
    unset($_SESSION[SYSTEM_SESSION]['eMsg']);
}

/*-!-!-!-!-!-!-!-!-*/

$mainBlock->assign('centerPanel',$centerPanelBlock);
$mainBlock->setBreadCrumbs($arrbreadCrumbs);
$mainBlock->assign('breadCrumbs',$mainBlock->breadCrumbs);
$mainBlock->displayBlock();


?>
