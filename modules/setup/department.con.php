<?php
require_once(SYSCONFIG_CLASS_PATH.'blocks/mainblock.class.php');
require_once(SYSCONFIG_CLASS_PATH.'admin/application.class.php');
require_once(SYSCONFIG_CLASS_PATH.'util/tablelist.class.php');
require_once(SYSCONFIG_CLASS_PATH.'setup/department.class.php');

Application::app_initialize(array("login_required"=> true));

$dbconn = Application::db_open();
//$dbconn->debug = true;

$cmap = array(
    "default" => "department.tpl.php",
    "add" => "department_form.tpl.php",
    "edit" => "department_form.tpl.php"
);

$cmapKey = (isset($_GET['action']))?$_GET['action']:"default";
$arrbreadCrumbs = array(
    'Setup' => ''
);

$cmapKey = isset($_GET['edit'])?"edit":$cmapKey;
$cmapKey = isset($_GET['delete'])?"delete":$cmapKey;

// Instantiate the MainBlock Class
$mainBlock = new MainBlock();
$mainBlock->assign('PageTitle','Department Setup');
$mainBlock->templateDir .= "admin";
$mainBlock->templateFile = "../index_sidebar_header.tpl.php";


// Instantiate the BlockBasePHP for Center Panel Display
$centerPanelBlock = new BlockBasePHP();
$centerPanelBlock->templateDir  .= "setup";
$centerPanelBlock->templateFile = $cmap[$cmapKey];

/*-!-!-!-!-!-!-!-!-*/

$objClsDepartmentSetup = new clsDepartmentSetup($dbconn);

switch ($cmapKey) {
    case 'add':
        $arrbreadCrumbs['Department'] = 'setup.php?inpage=department';
        $arrbreadCrumbs['Add New'] = "";
        $centerPanelBlock->assign("campusList", $objClsDepartmentSetup->getCampusList());
        if (count($_POST)>0) { $_POST = MainBlock::BlockSQLInjection($_POST);
            // save add new
            if(!$objClsDepartmentSetup->doValidateData($_POST)){
                $objClsDepartmentSetup->doPopulateData($_POST);
                $centerPanelBlock->assign("oData",$objClsDepartmentSetup->Data);
            }else {
                $objClsDepartmentSetup->doPopulateData($_POST);
                $objClsDepartmentSetup->doSaveAdd();
                header("Location: setup.php?inpage=department");
                exit;
            }
        }
        break;

    case 'edit':
        $arrbreadCrumbs['Department'] = 'setup.php?inpage=department';
        $arrbreadCrumbs['Edit'] = "";
        $centerPanelBlock->assign("campusList", $objClsDepartmentSetup->getCampusList());
        if (count($_POST)>0) { $_POST = MainBlock::BlockSQLInjection($_POST);
            // update
            if(!$objClsDepartmentSetup->doValidateData($_POST)){
                $objClsDepartmentSetup->doPopulateData($_POST);
                $centerPanelBlock->assign("oData",$objClsDepartmentSetup->Data);
            }else {
                $objClsDepartmentSetup->doPopulateData($_POST);
                $objClsDepartmentSetup->doSaveEdit();
                header("Location: setup.php?inpage=department");
                exit;
            }
        }else{
            $oData = $objClsDepartmentSetup->dbFetch($_GET['edit']);
            $centerPanelBlock->assign("oData",$oData);
        }
        break;

    case "update_status":
        $objClsDepartmentSetup->updateStatus($_GET['id']);
        exit();
        break;

    case "delete":
        $objClsDepartmentSetup->doDelete($_GET['delete']);
        header("Location: setup.php?inpage=department");
        exit;
        break;

    default:
        $arrbreadCrumbs['Department'] = "";
        $centerPanelBlock->assign('tblDataList',$objClsDepartmentSetup->getTableList());
        break;
}

if(isset($_SESSION[SYSTEM_SESSION]['eMsg'])){
    $mainBlock->assign('eMsg',$_SESSION[SYSTEM_SESSION]['eMsg']);
    unset($_SESSION[SYSTEM_SESSION]['eMsg']);
}

/*-!-!-!-!-!-!-!-!-*/

$mainBlock->assign('centerPanel',$centerPanelBlock);
$mainBlock->setBreadCrumbs($arrbreadCrumbs);
$mainBlock->assign('breadCrumbs',$mainBlock->breadCrumbs);
$mainBlock->displayBlock();


?>
