<?php
require_once(SYSCONFIG_CLASS_PATH.'blocks/mainblock.class.php');
require_once(SYSCONFIG_CLASS_PATH.'admin/application.class.php');
require_once(SYSCONFIG_CLASS_PATH.'util/tablelist.class.php');
require_once(SYSCONFIG_CLASS_PATH.'setup/site.class.php');

//Application::app_initialize(array("login_required"=> true));
Application::app_initialize();

$dbconn = Application::db_open();
//$dbconn->debug = true;

$cmap = array(
    "default" => "site.tpl.php",
    "add" => "site_form.tpl.php",
    "edit" => "site_form.tpl.php"
);

$cmapKey = (isset($_GET['action']))?$_GET['action']:"default";
$arrbreadCrumbs = array(
    'Setup' => ''
);

$cmapKey = isset($_GET['edit'])?"edit":$cmapKey;
$cmapKey = isset($_GET['delete'])?"delete":$cmapKey;
$cmapKey = isset($_GET['image'])?"image":$cmapKey;

// Instantiate the MainBlock Class
$mainBlock = new MainBlock();
$mainBlock->assign('PageTitle','Site Configuration');
$mainBlock->templateDir .= "admin";
$mainBlock->templateFile = "../index_sidebar_header.tpl.php";


// Instantiate the BlockBasePHP for Center Panel Display
$centerPanelBlock = new BlockBasePHP();
$centerPanelBlock->templateDir  .= "setup";
$centerPanelBlock->templateFile = $cmap[$cmapKey];

/*-!-!-!-!-!-!-!-!-*/

$objClsSiteSetup = new clsSiteSetup($dbconn);

switch ($cmapKey) {
    case "image":
        header("Content-type: image/jpeg");
        print $objClsSiteSetup->getPhoto($_GET);
        exit;
        break;

    case 'add':
        $arrbreadCrumbs['Site Configuration'] = 'setup.php?inpage=site';
        $arrbreadCrumbs['Add New'] = "";
        MainBlock::getUserOutOfPage();
        if (count($_POST)>0) { $_POST = MainBlock::BlockSQLInjection($_POST);
            // save add new
            if(!$objClsSiteSetup->doValidateData($_POST)){
                $objClsSiteSetup->doPopulateData($_POST);
                $centerPanelBlock->assign("oData",$objClsSiteSetup->Data);
            }else {
                $objClsSiteSetup->doPopulateData($_POST);
                $objClsSiteSetup->doSaveAdd();
                header("Location: setup.php?inpage=site");
                exit;
            }
        }
        break;

    case 'edit':
        $arrbreadCrumbs['Site Configuration'] = 'setup.php?inpage=site';
        $arrbreadCrumbs['Edit'] = "";
        MainBlock::getUserOutOfPage();
        if (count($_POST)>0) { $_POST = MainBlock::BlockSQLInjection($_POST);
            // update
            if(!$objClsSiteSetup->doValidateData($_POST)){
                $objClsSiteSetup->doPopulateData($_POST);
                $centerPanelBlock->assign("oData",$objClsSiteSetup->Data);
            }else {
                $objClsSiteSetup->doPopulateData($_POST);
                $objClsSiteSetup->doSaveEdit();
                header("Location: setup.php?inpage=site");
                exit;
            }
        }else{
            $oData = $objClsSiteSetup->dbFetch($_GET['edit']);
            $centerPanelBlock->assign("oData",$oData);
        }
        break;

    case "update_status":
        $objClsSiteSetup->updateStatus($_GET['id']);
        exit();
        break;

    case "delete":
        MainBlock::getUserOutOfPage();
        $objClsSiteSetup->doDelete($_GET['delete']);
        header("Location: setup.php?inpage=site");
        exit;
        break;

    default:
        $arrbreadCrumbs['Site Configuration'] = "";
        MainBlock::getUserOutOfPage();
        $centerPanelBlock->assign('tblDataList',$objClsSiteSetup->getTableList());
        break;
}

if(isset($_SESSION[SYSTEM_SESSION]['eMsg'])){
    $mainBlock->assign('eMsg',$_SESSION[SYSTEM_SESSION]['eMsg']);
    unset($_SESSION[SYSTEM_SESSION]['eMsg']);
}

/*-!-!-!-!-!-!-!-!-*/

$mainBlock->assign('centerPanel',$centerPanelBlock);
$mainBlock->setBreadCrumbs($arrbreadCrumbs);
$mainBlock->assign('breadCrumbs',$mainBlock->breadCrumbs);
$mainBlock->displayBlock();


?>
