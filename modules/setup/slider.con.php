<?php
require_once(SYSCONFIG_CLASS_PATH.'blocks/mainblock.class.php');
require_once(SYSCONFIG_CLASS_PATH.'admin/application.class.php');
require_once(SYSCONFIG_CLASS_PATH.'util/tablelist.class.php');
require_once(SYSCONFIG_CLASS_PATH.'setup/slider.class.php');

//Application::app_initialize(array("login_required"=> true));
Application::app_initialize();

$dbconn = Application::db_open();
//$dbconn->debug = true;

$cmap = array(
    "default" => "slider.tpl.php",
    "add" => "slider_form.tpl.php",
    "edit" => "slider_form.tpl.php"
);

$cmapKey = (isset($_GET['action']))?$_GET['action']:"default";
$arrbreadCrumbs = array(
    'Setup' => ''
);

$cmapKey = isset($_GET['edit'])?"edit":$cmapKey;
$cmapKey = isset($_GET['delete'])?"delete":$cmapKey;
$cmapKey = isset($_GET['image'])?"image":$cmapKey;

// Instantiate the MainBlock Class
$mainBlock = new MainBlock();
$mainBlock->assign('PageTitle','Site Slider Configuration');
$mainBlock->templateDir .= "admin";
$mainBlock->templateFile = "../index_sidebar_header.tpl.php";


// Instantiate the BlockBasePHP for Center Panel Display
$centerPanelBlock = new BlockBasePHP();
$centerPanelBlock->templateDir  .= "setup";
$centerPanelBlock->templateFile = $cmap[$cmapKey];

/*-!-!-!-!-!-!-!-!-*/

$objClsSliderSetup = new clsSliderSetup($dbconn);

switch ($cmapKey) {
    case "image":
        header("Content-type: image/jpeg");
        print $objClsSliderSetup->getPhoto($_GET);
        exit;
        break;

    case 'add':
        $arrbreadCrumbs['Site Slider Configuration'] = 'setup.php?inpage=slider';
        $arrbreadCrumbs['Add New'] = "";
        MainBlock::getUserOutOfPage();
        if (count($_POST)>0) { $_POST = MainBlock::BlockSQLInjection($_POST);
            // save add new
            if(!$objClsSliderSetup->doValidateData($_POST)){
                $objClsSliderSetup->doPopulateData($_POST);
                $centerPanelBlock->assign("oData",$objClsSliderSetup->Data);
            }else {
                $objClsSliderSetup->doPopulateData($_POST);
                $objClsSliderSetup->doSaveAdd();
                header("Location: setup.php?inpage=slider");
                exit;
            }
        }
        break;

    case 'edit':
        $arrbreadCrumbs['Site Slider Configuration'] = 'setup.php?inpage=slider';
        $arrbreadCrumbs['Edit'] = "";
        MainBlock::getUserOutOfPage();
        if (count($_POST)>0) { $_POST = MainBlock::BlockSQLInjection($_POST);
            // update
            if(!$objClsSliderSetup->doValidateData($_POST)){
                $objClsSliderSetup->doPopulateData($_POST);
                $centerPanelBlock->assign("oData",$objClsSliderSetup->Data);
            }else {
                $objClsSliderSetup->doPopulateData($_POST);
                $objClsSliderSetup->doSaveEdit();
                header("Location: setup.php?inpage=slider");
                exit;
            }
        }else{
            $oData = $objClsSliderSetup->dbFetch($_GET['edit']);
            $centerPanelBlock->assign("oData",$oData);
        }
        break;

    case "update_status":
        $objClsSliderSetup->updateStatus($_GET['id']);
        exit();
        break;

    case "delete":
        MainBlock::getUserOutOfPage();
        $objClsSliderSetup->doDelete($_GET['delete']);
        header("Location: setup.php?inpage=slider");
        exit;
        break;

    default:
        $arrbreadCrumbs['Site Slider Configuration'] = "";
        MainBlock::getUserOutOfPage();
        $centerPanelBlock->assign('tblDataList',$objClsSliderSetup->getList());
        break;
}

if(isset($_SESSION[SYSTEM_SESSION]['eMsg'])){
    $mainBlock->assign('eMsg',$_SESSION[SYSTEM_SESSION]['eMsg']);
    unset($_SESSION[SYSTEM_SESSION]['eMsg']);
}

/*-!-!-!-!-!-!-!-!-*/

$mainBlock->assign('centerPanel',$centerPanelBlock);
$mainBlock->setBreadCrumbs($arrbreadCrumbs);
$mainBlock->assign('breadCrumbs',$mainBlock->breadCrumbs);
$mainBlock->displayBlock();


?>
