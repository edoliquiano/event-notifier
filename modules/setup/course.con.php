<?php
require_once(SYSCONFIG_CLASS_PATH.'blocks/mainblock.class.php');
require_once(SYSCONFIG_CLASS_PATH.'admin/application.class.php');
require_once(SYSCONFIG_CLASS_PATH.'util/tablelist.class.php');
require_once(SYSCONFIG_CLASS_PATH.'setup/course.class.php');

Application::app_initialize(array("login_required"=> true));

$dbconn = Application::db_open();
//$dbconn->debug = true;

$cmap = array(
    "default" => "course.tpl.php",
    "add" => "course_form.tpl.php",
    "edit" => "course_form.tpl.php"
);

$cmapKey = (isset($_GET['action']))?$_GET['action']:"default";
$arrbreadCrumbs = array(
    'Setup' => ''
);

$cmapKey = isset($_GET['edit'])?"edit":$cmapKey;
$cmapKey = isset($_GET['delete'])?"delete":$cmapKey;

// Instantiate the MainBlock Class
$mainBlock = new MainBlock();
$mainBlock->assign('PageTitle','Course Setup');
$mainBlock->templateDir .= "admin";
$mainBlock->templateFile = "../index_sidebar_header.tpl.php";


// Instantiate the BlockBasePHP for Center Panel Display
$centerPanelBlock = new BlockBasePHP();
$centerPanelBlock->templateDir  .= "setup";
$centerPanelBlock->templateFile = $cmap[$cmapKey];

/*-!-!-!-!-!-!-!-!-*/

$objClsCourseSetup = new clsCourseSetup($dbconn);

switch ($cmapKey) {
    case 'add':
        $arrbreadCrumbs['Course'] = 'setup.php?inpage=course';
        $arrbreadCrumbs['Add New'] = "";
        $centerPanelBlock->assign("departmentList", $objClsCourseSetup->getDepartmentList());
        if (count($_POST)>0) { $_POST = MainBlock::BlockSQLInjection($_POST);
            // save add new
            if(!$objClsCourseSetup->doValidateData($_POST)){
                $objClsCourseSetup->doPopulateData($_POST);
                $centerPanelBlock->assign("oData",$objClsCourseSetup->Data);
            }else {
                $objClsCourseSetup->doPopulateData($_POST);
                $objClsCourseSetup->doSaveAdd();
                header("Location: setup.php?inpage=course");
                exit;
            }
        }
        break;

    case 'edit':
        $arrbreadCrumbs['Course'] = 'setup.php?inpage=course';
        $arrbreadCrumbs['Edit'] = "";
        $centerPanelBlock->assign("departmentList", $objClsCourseSetup->getDepartmentList());
        if (count($_POST)>0) { $_POST = MainBlock::BlockSQLInjection($_POST);
            // update
            if(!$objClsCourseSetup->doValidateData($_POST)){
                $objClsCourseSetup->doPopulateData($_POST);
                $centerPanelBlock->assign("oData",$objClsCourseSetup->Data);
            }else {
                $objClsCourseSetup->doPopulateData($_POST);
                $objClsCourseSetup->doSaveEdit();
                header("Location: setup.php?inpage=course");
                exit;
            }
        }else{
            $oData = $objClsCourseSetup->dbFetch($_GET['edit']);
            $centerPanelBlock->assign("oData",$oData);
        }
        break;

    case "update_status":
        $objClsCourseSetup->updateStatus($_GET['id']);
        exit();
        break;

    case "delete":
        $objClsCourseSetup->doDelete($_GET['delete']);
        header("Location: setup.php?inpage=course");
        exit;
        break;

    default:
        $arrbreadCrumbs['Course'] = "";
        $centerPanelBlock->assign('tblDataList',$objClsCourseSetup->getTableList());
        break;
}

if(isset($_SESSION[SYSTEM_SESSION]['eMsg'])){
    $mainBlock->assign('eMsg',$_SESSION[SYSTEM_SESSION]['eMsg']);
    unset($_SESSION[SYSTEM_SESSION]['eMsg']);
}

/*-!-!-!-!-!-!-!-!-*/

$mainBlock->assign('centerPanel',$centerPanelBlock);
$mainBlock->setBreadCrumbs($arrbreadCrumbs);
$mainBlock->assign('breadCrumbs',$mainBlock->breadCrumbs);
$mainBlock->displayBlock();


?>
