<?php
require_once(SYSCONFIG_CLASS_PATH.'blocks/mainblock.class.php');
require_once(SYSCONFIG_CLASS_PATH.'admin/application.class.php');
require_once(SYSCONFIG_CLASS_PATH.'util/tablelist.class.php');
require_once(SYSCONFIG_CLASS_PATH.'setup/campus.class.php');

Application::app_initialize(array("login_required"=> true));

$dbconn = Application::db_open();
//$dbconn->debug = true;

$cmap = array(
    "default" => "campus.tpl.php",
    "add" => "campus_form.tpl.php",
    "edit" => "campus_form.tpl.php"
);

$cmapKey = (isset($_GET['action']))?$_GET['action']:"default";
$arrbreadCrumbs = array(
    'Setup' => ''
);

$cmapKey = isset($_GET['edit'])?"edit":$cmapKey;
$cmapKey = isset($_GET['delete'])?"delete":$cmapKey;

// Instantiate the MainBlock Class
$mainBlock = new MainBlock();
$mainBlock->assign('PageTitle','Campus Setup');
$mainBlock->templateDir .= "admin";
$mainBlock->templateFile = "../index_sidebar_header.tpl.php";


// Instantiate the BlockBasePHP for Center Panel Display
$centerPanelBlock = new BlockBasePHP();
$centerPanelBlock->templateDir  .= "setup";
$centerPanelBlock->templateFile = $cmap[$cmapKey];

/*-!-!-!-!-!-!-!-!-*/

$objClsCampusSetup = new clsCampusSetup($dbconn);

switch ($cmapKey) {
    case 'add':
        $arrbreadCrumbs['Campus'] = 'setup.php?inpage=campus';
        $arrbreadCrumbs['Add New'] = "";
        if (count($_POST)>0) { $_POST = MainBlock::BlockSQLInjection($_POST);
            // save add new
            if(!$objClsCampusSetup->doValidateData($_POST)){
                $objClsCampusSetup->doPopulateData($_POST);
                $centerPanelBlock->assign("oData",$objClsCampusSetup->Data);
            }else {
                $objClsCampusSetup->doPopulateData($_POST);
                $objClsCampusSetup->doSaveAdd();
                header("Location: setup.php?inpage=campus");
                exit;
            }
        }
        break;

    case 'edit':
        $arrbreadCrumbs['Campus'] = 'setup.php?inpage=campus';
        $arrbreadCrumbs['Edit'] = "";
        
        if (count($_POST)>0) { $_POST = MainBlock::BlockSQLInjection($_POST);
            // update
            if(!$objClsCampusSetup->doValidateData($_POST)){
                $objClsCampusSetup->doPopulateData($_POST);
                $centerPanelBlock->assign("oData",$objClsCampusSetup->Data);
            }else {
                $objClsCampusSetup->doPopulateData($_POST);
                $objClsCampusSetup->doSaveEdit();
                header("Location: setup.php?inpage=campus");
                exit;
            }
        }else{
            $oData = $objClsCampusSetup->dbFetch($_GET['edit']);
            $centerPanelBlock->assign("oData",$oData);
        }
        break;

    case "update_status":
        $objClsCampusSetup->updateStatus($_GET['id']);
        exit();
        break;

    case "delete":
        $objClsCampusSetup->doDelete($_GET['delete']);
        header("Location: setup.php?inpage=campus");
        exit;
        break;

    default:
        $arrbreadCrumbs['Campus'] = "";
        $centerPanelBlock->assign('tblDataList',$objClsCampusSetup->getTableList());
        break;
}

if(isset($_SESSION[SYSTEM_SESSION]['eMsg'])){
    $mainBlock->assign('eMsg',$_SESSION[SYSTEM_SESSION]['eMsg']);
    unset($_SESSION[SYSTEM_SESSION]['eMsg']);
}

/*-!-!-!-!-!-!-!-!-*/

$mainBlock->assign('centerPanel',$centerPanelBlock);
$mainBlock->setBreadCrumbs($arrbreadCrumbs);
$mainBlock->assign('breadCrumbs',$mainBlock->breadCrumbs);
$mainBlock->displayBlock();


?>
