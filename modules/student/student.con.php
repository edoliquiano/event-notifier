<?php
require_once(SYSCONFIG_CLASS_PATH.'blocks/mainblock.class.php');
require_once(SYSCONFIG_CLASS_PATH.'admin/application.class.php');
require_once(SYSCONFIG_CLASS_PATH.'util/tablelist.class.php');
require_once(SYSCONFIG_CLASS_PATH.'student/student.class.php');

Application::app_initialize(array("login_required"=> true));

$dbconn = Application::db_open();
//$dbconn->debug = true;

$cmap = array(
    "default" => "student.tpl.php",
    "add" => "student_form.tpl.php",
    "edit" => "student_form.tpl.php"
);

$cmapKey = (isset($_GET['action']))?$_GET['action']:"default";
$arrbreadCrumbs = array(
    'Setup' => ''
);

$cmapKey = isset($_GET['edit'])?"edit":$cmapKey;
$cmapKey = isset($_GET['delete'])?"delete":$cmapKey;

// Instantiate the MainBlock Class
$mainBlock = new MainBlock();
$mainBlock->assign('PageTitle','Student List');
$mainBlock->templateDir .= "admin";
$mainBlock->templateFile = "../index_sidebar_header.tpl.php";


// Instantiate the BlockBasePHP for Center Panel Display
$centerPanelBlock = new BlockBasePHP();
$centerPanelBlock->templateDir  .= "student";
$centerPanelBlock->templateFile = $cmap[$cmapKey];

/*-!-!-!-!-!-!-!-!-*/

$objClsStudentList = new clsStudentList($dbconn);

$centerPanelBlock->assign('campusList', $objClsStudentList->getCampusList());
$centerPanelBlock->assign('departmentList', $objClsStudentList->getDepartmentList());
$centerPanelBlock->assign('courseList', $objClsStudentList->getCourseList());

switch ($cmapKey) {
    case 'add':
        $arrbreadCrumbs['Student List'] = 'student.php?inpage=student';
        $arrbreadCrumbs['Add New'] = "";
        if (count($_POST)>0) { $_POST = MainBlock::BlockSQLInjection($_POST);
            // save add new
            if(!$objClsStudentList->doValidateData($_POST)){
                $objClsStudentList->doPopulateData($_POST);
                $centerPanelBlock->assign("oData",$objClsStudentList->Data);
            }else {
                $objClsStudentList->doPopulateData($_POST);
                $objClsStudentList->doSaveAdd();
                header("Location: student.php?inpage=student");
                exit;
            }
        }
        break;

    case 'edit':
        $arrbreadCrumbs['Student List'] = 'student.php?inpage=student';
        $arrbreadCrumbs['Edit'] = "";

        if (count($_POST)>0) { $_POST = MainBlock::BlockSQLInjection($_POST);
            // update
            if(!$objClsStudentList->doValidateData($_POST)){
                $objClsStudentList->doPopulateData($_POST);
                $centerPanelBlock->assign("oData",$objClsStudentList->Data);
            }else {
                $objClsStudentList->doPopulateData($_POST);
                $objClsStudentList->doSaveEdit();
                header("Location: student.php?inpage=student");
                exit;
            }
        }else{
            $oData = $objClsStudentList->dbFetch($_GET['edit']);
            $centerPanelBlock->assign("oData",$oData);
        }
        break;

    case "update_status":
        $objClsStudentList->updateStatus($_GET['id']);
        exit();
        break;

    case "delete":
        $objClsStudentList->doDelete($_GET['delete']);
        header("Location: student.php?inpage=student");
        exit;
        break;

    default:
        $arrbreadCrumbs['Student List'] = "";
        $centerPanelBlock->assign('tblDataList',$objClsStudentList->getTableList());
        break;
}

if(isset($_SESSION[SYSTEM_SESSION]['eMsg'])){
    $mainBlock->assign('eMsg',$_SESSION[SYSTEM_SESSION]['eMsg']);
    unset($_SESSION[SYSTEM_SESSION]['eMsg']);
}

/*-!-!-!-!-!-!-!-!-*/

$mainBlock->assign('centerPanel',$centerPanelBlock);
$mainBlock->setBreadCrumbs($arrbreadCrumbs);
$mainBlock->assign('breadCrumbs',$mainBlock->breadCrumbs);
$mainBlock->displayBlock();


?>
