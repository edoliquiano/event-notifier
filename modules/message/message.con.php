<?php
require_once(SYSCONFIG_CLASS_PATH.'blocks/mainblock.class.php');
require_once(SYSCONFIG_CLASS_PATH.'admin/application.class.php');
require_once(SYSCONFIG_CLASS_PATH.'util/tablelist.class.php');
require_once(SYSCONFIG_CLASS_PATH.'message/message.class.php');

Application::app_initialize(array("login_required"=> true));

$dbconn = Application::db_open();
//$dbconn->debug = true;

$cmap = array(
    "default" => "message.tpl.php",
    "add" => "message_form.tpl.php",
    "view" => "message_form.tpl.php"
);

$cmapKey = (isset($_GET['action']))?$_GET['action']:"default";
$arrbreadCrumbs = array(
    'Setup' => ''
);

$cmapKey = isset($_GET['delete'])?"delete":$cmapKey;

// Instantiate the MainBlock Class
$mainBlock = new MainBlock();
$mainBlock->assign('PageTitle','Message Setup');
$mainBlock->templateDir .= "admin";
$mainBlock->templateFile = "../index_sidebar_header.tpl.php";


// Instantiate the BlockBasePHP for Center Panel Display
$centerPanelBlock = new BlockBasePHP();
$centerPanelBlock->templateDir  .= "message";
$centerPanelBlock->templateFile = $cmap[$cmapKey];

/*-!-!-!-!-!-!-!-!-*/

$objClsMessageSetup = new clsMessageSetup($dbconn);

switch ($cmapKey) {
    case 'add':
        $arrbreadCrumbs['Message'] = 'message.php?inpage=message';
        $arrbreadCrumbs['Add New'] = "";
        if (count($_POST)>0) { $_POST = MainBlock::BlockSQLInjection($_POST);
            // save add new
            if(!$objClsMessageSetup->doValidateData($_POST)){
                $objClsMessageSetup->doPopulateData($_POST);
                $centerPanelBlock->assign("oData",$objClsMessageSetup->Data);
            }else {
                $objClsMessageSetup->doPopulateData($_POST);
                $objClsMessageSetup->doSaveAdd();
                header("Location: message.php?inpage=message");
                exit;
            }
        }
        break;

    case "delete":
        $objClsMessageSetup->doDelete($_GET['delete']);
        header("Location: message.php?inpage=message");
        exit;
        break;

    case 'url_send':
        $expliode_ids = explode(',', $_GET['student_id']);
        $objClsMessageSetup->updateStudentsSms($_GET['text'], $expliode_ids);
        exit();
        break;

    default:
        $arrbreadCrumbs['Message'] = "";
        $centerPanelBlock->assign('tblDataList',$objClsMessageSetup->getTableList());
        break;
}

if(isset($_SESSION[SYSTEM_SESSION]['eMsg'])){
    $mainBlock->assign('eMsg',$_SESSION[SYSTEM_SESSION]['eMsg']);
    unset($_SESSION[SYSTEM_SESSION]['eMsg']);
}

/*-!-!-!-!-!-!-!-!-*/

$mainBlock->assign('centerPanel',$centerPanelBlock);
$mainBlock->setBreadCrumbs($arrbreadCrumbs);
$mainBlock->assign('breadCrumbs',$mainBlock->breadCrumbs);
$mainBlock->displayBlock();


?>
