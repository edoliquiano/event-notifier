<?php
//ini_set('error_reporting', E_ALL, E_NOTICE,E_WARNING);
ini_set('error_reporting', 0);
ini_set('display_errors', true);
session_start();

define('SYSCONFIG_ISLOCALCONN',true);

if(SYSCONFIG_ISLOCALCONN){
	define('SYSCONFIG_DBUSER', 'root');
	define('SYSCONFIG_DBPASS', 'r00tb33r');
	define('SYSCONFIG_DBNAME', 'db_event_notif');
	define('SYSCONFIG_DBHOST', 'localhost');
}else{
	define('SYSCONFIG_DBUSER', 'boot');
	define('SYSCONFIG_DBPASS', 'r00tb33r');
	define('SYSCONFIG_DBNAME', 'db_event_notif');
	define('SYSCONFIG_DBHOST', '192.168.1.210');
}

define('SYSCONFIG_TITLE',($_SESSION['buena_session']['site_setup']['site_title'] != '' ? $_SESSION['buena_session']['site_setup']['site_title'] : $_SESSION['buena_session']['site_setup']['site_title']));
define('SYSTEM_SESSION','buena_session');
define('SYSTEM_ICON',($_SESSION['buena_session']['site_setup']['site_logo'] != '' ? $_SESSION['buena_session']['site_setup']['site_logo'] : $_SESSION['buena_session']['site_setup']['site_logo']));
define('SYSTEM_DESC',($_SESSION['buena_session']['site_setup']['site_desc'] != '' ? $_SESSION['buena_session']['site_setup']['site_desc'] : $_SESSION['buena_session']['site_setup']['site_desc']));

define('SYSCONFIG_THEME', 'default');

function printa($arr = array()){
	print "<pre>";
	print_r($arr);
	print "</pre>";
}


?>
