<!-- Bootstrap -->
<link href="../components/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="../components/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="../components/vendors/nprogress/nprogress.css" rel="stylesheet">
<!-- FullCalendar -->
<link href="../components/vendors/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
<link href="../components/vendors/fullcalendar/dist/fullcalendar.print.css" rel="stylesheet" media="print">
<!-- iCheck -->
<link href="../components/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
<!-- Select2 -->
<link href="../components/vendors/select2/dist/css/select2.css" rel="stylesheet">
<!-- Switchery -->
<link href="../components/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
<!-- bootstrap-daterangepicker -->
<link href="../components/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<!-- bootstrap-datetimepicker -->
<link href="../components/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
<!-- Custom styling plus plugins -->
<link href="../components/build/css/custom.min.css" rel="stylesheet">