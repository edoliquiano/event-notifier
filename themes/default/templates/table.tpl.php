<?php
if (!empty($_SERVER['QUERY_STRING'])) {
    $qrystr = explode("&", $_SERVER['QUERY_STRING']);
    foreach ($qrystr as $value) {
        $qstr = explode("=", $value);
        if ($qstr[0] != "sortby" && $qstr[0] != "sortof") {
            $arrQryStr[] = implode("=", $qstr);
        }
        if ($qstr[0] != "search_field" && $qstr[0] != "p") {
            $qryFrm[] = "<input type='hidden' name='$qstr[0]' value='" . urldecode($qstr[1]) . "'>";
        }
    }
    $aQryStr = $arrQryStr;
    $aQryStr[] = "p=@@";
    $qryForms = (count($qryFrm) > 0) ? implode(" ", $qryFrm) : '';
}

$srchFormAction = $_SERVER['PHP_SELF'] . "?$queryStr";
?>

<div class="row">
    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
        <div class="pull-left">
            <?= $resultsInfo ?>
        </div>
    </div>
    <div style="margin-top: 1.8%;" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 pull-right top_search">
        <form method="GET" action="" class="form-horizontal">
            <div class="input-group">
                <input type="text" name="search_field" class="form-control" placeholder="Search for..." value="<?=MainBlock::BlockSQLInjection($_REQUEST['search_field'])?>">
                <span class="input-group-btn">
                    <input class="btn btn-default" type="submit" value="Go!" style="display: none;" >
                    <button type="button" onclick="this.form.submit()" class="btn btn-default">Go!</button>
                </span>
            </div>
            <?= $qryForms ?>
        </form>
    </div>
</div>

<form role="form" method="get" action="">
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 center-block">
                            <input type="text" autofocus class="form-control input-lg" placeholder="SEARCH" name="search_field">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 center-block">
                           <button type="submit" class="btn btn-primary btn-lg" style="width: 100%">SEARCH</button>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 center-block">
                            <button type="button" class="btn btn-danger btn-lg" onclick="closeModal()" style="width: 100%">CANCEL</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?= $qryForms ?>
</form>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <table class="table table-striped jambo_table bulk_action">
            <thead>
                <tr class="headings">
                    <?php
                    foreach ($tblFields as $fkey => $fvalue) {
                        $aQryStr = $arrQryStr;
                        $aQryStr[] = "sortby=$fkey";
                        $aQryStr[] = "sortof=" . ((isset($_GET['sortof']) && $_GET['sortof'] == "asc") ? "desc" : "asc");
                        $queryStr = implode("&", $aQryStr);
//                        $fvalue = (empty($fvalue)) ? "" : "<span onclick='window.location.href=\"?{$queryStr}\"' style=\"cursor: pointer\">$fvalue</span>";
//                        $fvalue = (empty($fvalue)) ? "" : "<a href='?$queryStr' class='label' style='font-size: 13px;'>$fvalue</a>";
                        $sortimg = isset($_GET['sortof']) ? (($_GET['sortof'] == "asc") ? "fa-sort-amount-asc" : "fa-sort-amount-desc") : "";
                        $sortimg = ($fkey == $_GET['sortby']) ? ' <i class="fa '.$sortimg.'"></i>' : "";
                        $fvalue = (empty($fvalue)) ? "" : "<a href='?$queryStr' class='label' style='font-size: 13px;'>$fvalue$sortimg</a>";

                        ?>
                        <th><?= $fvalue/* . $sortimg */?></th>
                    <?php
                    }
                    ?>
                </tr>
            </thead>
            <tbody>

            <?php
            if (count($tblData) > 0) {
                foreach ($tblData as $dkey => $dvalue) {
                    ?>
                    <tr>
                        <?php
                        foreach ($tblFields as $fkey => $fvalue) {
                            ?>
                            <td  <?= (isset($attribs[$fkey]) ? $attribs[$fkey] : "") ?> ><?= $dvalue[$fkey] ?></td>
                        <?php
                        }
                        ?>
                    </tr>
                <?php
                }
            } else {
                ?>
                <tr>
                    <td colspan="<?= count($tblFields) ?>" style="text-align: center;"><h5>No record found.</h5></td>
                </tr>
            <?php
            }
            ?>
            </tbody>

        </table>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="pull-left">
            <?= $resultsInfo ?>
        </div>
    </div>
</div>

<script language="JavaScript">
    function closeModal() {
        $(function () {
            $('#myModal').modal('toggle');
        });
    }
</script>

