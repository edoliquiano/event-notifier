<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/png" href="<?=SYSTEM_ICON?>"/>

        <title><?= SYSCONFIG_TITLE ?></title>

<!--        <style type="text/css">-->
<!--            @import url('../site.css');-->
<!--        </style>-->

        <?php
            require_once('../site.php');
        ?>
    </head>

    <?php
        require_once("../scripts_top.php");
    ?>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">
                        <div class="navbar nav_title" style="border: 0;">
                            <div id="sideBarLogo">

                            </div>
                            <!--                    <center><img align="" src="--><?//=$_SESSION['hotel_site_settings']['conf_client_logo_rectangle']?><!--" class="img-responsive" alt="" style="width:100%;height:60px;display: block; "></center>-->
                            <!--                    <a href="index.php" class="site_title"><i class="fa fa-circle"></i> <span>Hotel</span></a>-->
                        </div>

                        <div class="clearfix"></div>

                        <!-- menu profile quick info -->
                        <!--                <div class="profile clearfix">-->
                        <!--                    <div class="profile_pic">-->
                        <!--                        <img src="../components/images/img.jpg" alt="..." class="img-circle profile_img">-->
                        <!--                    </div>-->
                        <!--                    <div class="profile_info">-->
                        <!--                        <span>Welcome,</span>-->
                        <!--                        <h2>John Doe</h2>-->
                        <!--                    </div>-->
                        <!--                    <div class="clearfix"></div>-->
                        <!--                </div>-->
                        <!-- /menu profile quick info -->

                        <br />

                        <!-- sidebar menu -->
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                            <div class="menu_section">
                                <?=$_SESSION[SYSTEM_SESSION]['menuinfo']['default']?>
                            </div>
                        </div>
                        <!-- /sidebar menu -->

                        <!-- /menu footer buttons -->
                        <div class="sidebar-footer hidden-small">
                            <a data-toggle="tooltip" data-placement="top" style="width: 100%;" title="Logout" href="admin.php?inpage=logout">
                                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                            </a>
                        </div>
                        <!-- /menu footer buttons -->
                    </div>
                </div>

                <!-- top navigation -->
                <div class="top_nav">
                    <div class="nav_menu">
                        <nav>
                            <div class="nav toggle">
                                <a id="menu_toggle" onclick="updateSideBar(1)"><i class="fa fa-bars"></i></a>
                            </div>
                            <?=$_SESSION[SYSTEM_SESSION]['menuinfotop']['default']?>

                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <!--<img src="../components/images/img.jpg" alt="">--><?=$_SESSION[SYSTEM_SESSION]['user_name']?>
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                                        <li><a href="setup.php?inpage=account&edit=<?=$_SESSION[SYSTEM_SESSION]['user_id']?>"><i class="fa fa-cogs pull-right"></i> Account Setup</a></li>
                                        <li><a href="admin.php?inpage=logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                                    </ul>
                                </li>

<!--                                <li role="presentation" class="dropdown">-->
<!--                                    <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">-->
<!--                                        <i class="fa fa-envelope-o"></i>-->
<!--                                        <span class="badge bg-green">6</span>-->
<!--                                    </a>-->
<!--                                    <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">-->
<!--                                        <li>-->
<!--                                            <a>-->
<!--                                                <span class="image"><img src="../components/images/img.jpg" alt="Profile Image" /></span>-->
<!--                        <span>-->
<!--                          <span>John Smith</span>-->
<!--                          <span class="time">3 mins ago</span>-->
<!--                        </span>-->
<!--                        <span class="message">-->
<!--                          Film festivals used to be do-or-die moments for movie makers. They were where...-->
<!--                        </span>-->
<!--                                            </a>-->
<!--                                        </li>-->
<!--                                        <li>-->
<!--                                            <a>-->
<!--                                                <span class="image"><img src="../components/images/img.jpg" alt="Profile Image" /></span>-->
<!--                        <span>-->
<!--                          <span>John Smith</span>-->
<!--                          <span class="time">3 mins ago</span>-->
<!--                        </span>-->
<!--                        <span class="message">-->
<!--                          Film festivals used to be do-or-die moments for movie makers. They were where...-->
<!--                        </span>-->
<!--                                            </a>-->
<!--                                        </li>-->
<!--                                        <li>-->
<!--                                            <a>-->
<!--                                                <span class="image"><img src="../components/images/img.jpg" alt="Profile Image" /></span>-->
<!--                        <span>-->
<!--                          <span>John Smith</span>-->
<!--                          <span class="time">3 mins ago</span>-->
<!--                        </span>-->
<!--                        <span class="message">-->
<!--                          Film festivals used to be do-or-die moments for movie makers. They were where...-->
<!--                        </span>-->
<!--                                            </a>-->
<!--                                        </li>-->
<!--                                        <li>-->
<!--                                            <a>-->
<!--                                                <span class="image"><img src="../components/images/img.jpg" alt="Profile Image" /></span>-->
<!--                        <span>-->
<!--                          <span>John Smith</span>-->
<!--                          <span class="time">3 mins ago</span>-->
<!--                        </span>-->
<!--                        <span class="message">-->
<!--                          Film festivals used to be do-or-die moments for movie makers. They were where...-->
<!--                        </span>-->
<!--                                            </a>-->
<!--                                        </li>-->
<!--                                        <li>-->
<!--                                            <div class="text-center">-->
<!--                                                <a>-->
<!--                                                    <strong>See All Alerts</strong>-->
<!--                                                    <i class="fa fa-angle-right"></i>-->
<!--                                                </a>-->
<!--                                            </div>-->
<!--                                        </li>-->
<!--                                    </ul>-->
<!--                                </li>-->
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- /top navigation -->

                <!-- page content -->
                <div class="right_col" role="main">
                    <div class="">
                        <?php
                        if (!empty($breadCrumbs)) {
                            ?>
                </br></br></br>
                <div class="breadCrumbs01 text-right breadcrumb text-muted">
                    <!--<i class="fa fa-dashboard"></i> --><?= $breadCrumbs; ?>
                </div>
            <?php
                        }
                        ?>
                        <div class="row">
                            <?php

                            if(isset($eMsg)){
                                if (is_array($eMsg)) {
                                    ?>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="alert alert-danger alert-dismissible">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            <b>Check the following error(s) below:</b><br>
                                            <?php
                                            foreach ($eMsg as $key => $value) {
                                                ?>
                                                &nbsp;&nbsp;&bull;&nbsp;<?=$value?><br>
                                            <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                <?php
                                }else {
                                    ?>
                                    <div class="col-md-12">
                                        <div class="alert alert-success alert-dismissible">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            <?=$eMsg?>
                                        </div>
                                    </div>
                                <?php
                                }
                            }

                            if (is_object($centerPanel))
                                print $centerPanel->fetchBlock();
                            ?>
                            <div><?= $indexErrMsg ?></div>
                        </div>
                    </div>
                </div>

                <footer>
                    <div class="pull-right">
                       &nbsp;
                    </div>
                    <div class="clearfix"></div>
                </footer>

            </div>
        </div>
    </body>

    <?php
        require_once("../scripts_bottom.php");
    ?>
</html>
