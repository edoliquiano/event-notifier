<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="<?=SYSTEM_ICON?>"/>

    <title><?= SYSCONFIG_TITLE ?></title>

    <?php
        require_once('../site.php');
    ?>
</head>

<?php
    require_once("../scripts_top.php");
?>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <!-- top navigation -->
        <div class="top_nav" style="margin-left: 0%;">
            <div class="nav_menu">
                <nav>
                    <?=$_SESSION[SYSTEM_SESSION]['menuinfotop']['default']?>

                    <ul class="nav navbar-nav navbar-right">
                        <?php
                            if(isset($_GET['login'])){
                                ?>
                                <li <?=explode("/", $_SERVER['REQUEST_URI'])[3] == 'admin.php?login' ? 'class=\'active\'' : ''?>>
                                    <a href = "admin.php" data-toggle="tooltip" data-placement="left" title="Login"><i class="fa fa-user"></i></a>
                                </li>
                            <?php
                            }else{
                                ?>
                                <li <?=explode("/", $_SERVER['REQUEST_URI'])[3] == 'calendar.php' ? 'class=\'active\'' : ''?>>
                                    <a href = "calendar.php" data-toggle="tooltip" data-placement="left" title="Calendar"><i class="fa fa-calendar"></i></a>
                                </li>
                            <?php
                            }
                        ?>
                        <li <?=explode("/", $_SERVER['REQUEST_URI'])[3] == 'admin.php' ? 'class=\'active\'' : ''?>>
                            <a href = "admin.php" data-toggle="tooltip" data-placement="left" title="Login"><i class="fa fa-home"></i></a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main" style="margin-left: 0%;">
            <div class="">
                <?php
                if (!empty($breadCrumbs)) {
                    ?>
                </br></br></br>
                <div class="breadCrumbs01 text-right breadcrumb text-muted">
                    <!--<i class="fa fa-dashboard"></i> --><?= $breadCrumbs; ?>
                </div>
            <?php
                }
                ?>
                <div class="row">
                    <?php

                    if(isset($eMsg)){
                        if (is_array($eMsg)) {
                            ?>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <b>Check the following error(s) below:</b><br>
                                    <?php
                                    foreach ($eMsg as $key => $value) {
                                        ?>
                                        &nbsp;&nbsp;&bull;&nbsp;<?=$value?><br>
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        <?php
                        }else {
                            ?>
                            <div class="col-md-12">
                                <div class="alert alert-success alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?=$eMsg?>
                                </div>
                            </div>
                        <?php
                        }
                    }

                    if (is_object($centerPanel))
                        print $centerPanel->fetchBlock();
                    ?>
                    <div><?= $indexErrMsg ?></div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>

<?php
    require_once("../scripts_bottom.php");
?>
</html>
