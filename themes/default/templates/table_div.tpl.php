<style>
    .tblstyl{
        font-size: 2em;
    }
    h2{
        margin: 0px;
        font-weight: bold;
    }
    p{
        font-size: x-large;
        margin: 0px;
    }

    .link{
        color: #333;
    }
    .link:hover{
        color: #333;
        /*text-decoration: none;*/
    }
</style>
<?php
if (!empty($_SERVER['QUERY_STRING'])) {
	$qrystr = explode("&",$_SERVER['QUERY_STRING']);
	foreach ($qrystr as $value) {
		$qstr = explode("=",$value);
		if ($qstr[0]!="sortby" && $qstr[0]!="sortof") {
			$arrQryStr[] = implode("=", $qstr);
		}
		if ($qstr[0]!="search_field" && $qstr[0]!="p") {
			$qryFrm[] = "<input type='hidden' name='$qstr[0]' value='".urldecode($qstr[1])."'>";
		}
	}
	$aQryStr = $arrQryStr;
	$aQryStr[] = "p=@@";
	$qryForms = (count($qryFrm)>0)? implode(" ",$qryFrm) : '';
}

$srchFormAction = $_SERVER['PHP_SELF']."?$queryStr";

?>
<div class="row">
    <div class="col-sm-8">
        <div class="divTblContent" style="font-size: 17px"><?=$resultsInfo?></div>
    </div>
    <div class="col-sm-4">
        <div class="form-group input-group pull-right">
            <button class="btn btn-primary btn-lg pull-right" type="button"  data-toggle="modal" data-target="#myModal" onclick="">SEARCH <i class="fa fa-search"></i>
            </button>
        </div>
    </div>
</div>

<form role="form" method="get" action="">
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col-md-12 center-block">
                            <input type="text" autofocus class="form-control input-lg" placeholder="SEARCH" name="search_field">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6 col-xs-6 center-block">
                            <button type="submit" class="btn btn-primary btn-lg" style="width: 100%">SEARCH</button>
                        </div>
                        <div class="col-md-6 col-xs-6 center-block">
                            <button type="button" class="btn btn-danger btn-lg" onclick="closeModal()" style="width: 100%">CANCEL</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?=$qryForms?>
</form>

<div class="row">
<div class="col-lg-12 tblstyl" id="tblplaylist">
<?php
	if(count($tblData) > 0){
        $counter = 0;
  foreach ($tblData as $dkey => $dvalue){

  ?>
<!--  <div class="row" style="<?=$counter>0?'border-bottom: 1px solid #6E6E6E':'border-top: 1px solid #6E6E6E; border-bottom: 1px solid #6E6E6E'?>">-->
      <?=$counter==0?'<hr>':''?>
  <div class="row" style="">
    <?php
    $counter++;
    foreach($tblFields as $fkey => $fvalue){
    ?>
         <div <?=(isset($attribs[$fkey])?$attribs[$fkey]:"")?> ><?=$dvalue[$fkey]?></div>
    <?php
    }
    ?>
         
  </div>
    <hr>
  <?php
	}
	}else{
  ?>
	<div class="alert alert-warning">
	<h2><i class="fa fa-music"></i> No record found.</h2>
	</div>
	<?php
	}
	?>

    
</div>
</div>
  <div class="row">
      <div class="col-lg-12">
        <?=$resultsInfo?>  
      </div>
  </div>

<script language="JavaScript">
    function closeModal() {
        $(function () {
            $('#myModal').modal('toggle');
        });
    }

    $(function() {
        function reposition() {
            var modal = $(this),
                dialog = modal.find('.modal-dialog');
            modal.css('display', 'block');

            // Dividing by two centers the modal exactly, but dividing by three
            // or four works better for larger screens.
            dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));
        }
        // Reposition when a modal is shown
        $('.modal').on('show.bs.modal', reposition);
        // Reposition when the window is resized
        $(window).on('resize', function() {
            $('.modal:visible').each(reposition);
        });
    });
</script>

