<form method="post" id="toSubmit"></form>
<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="panel-title">Student List</div>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <label class="control-label">Given Name</label>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <input type="text" form="toSubmit" class="form-control" name="student_gname" value="<?=$oData['student_gname']?>" />
                </div>
            </div>
            <div class="form-group">
                <label class="control-label">Middle Name</label>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <input type="text" form="toSubmit" class="form-control" name="student_mname" value="<?=$oData['student_mname']?>" />
                </div>
            </div>
            <div class="form-group">
                <label class="control-label">Last Name</label>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <input type="text" form="toSubmit" class="form-control" name="student_lname" value="<?=$oData['student_lname']?>" />
                </div>
            </div>
            <div class="form-group">
                <label class="control-label">Address</label>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <textarea class="form-control" form="toSubmit" name="student_address" style="width: 100%; min-width: 100%; max-width: 100%;"><?=$oData['student_address']?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label">Mobile Number</label>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <input type="text" form="toSubmit" class="form-control" name="student_mobile" value="<?=$oData['student_mobile']?>" />
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <a href="?inpage=<?=$_GET['inpage']?>" class="btn btn-danger">Cancel</a>
            <input type="submit" class="btn btn-success pull-right" value="Save" form="toSubmit" />
        </div>
    </div>
</div>