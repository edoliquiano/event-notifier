<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Calendar Events <small>Sessions</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li style="color: white;">asddasd</li>
                    <li style="color: white;">asdasd</li>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <div id='calendar'></div>

            </div>
        </div>
    </div>
</div>

<!-- calendar modal -->
<div id="CalenderModalNew" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">New Calendar Entry</h4>
            </div>
            <div class="modal-body">
                <div id="testmodal" style="padding: 5px 20px;">
                    <form id="antoform" method="post" class="form-horizontal calender" role="form">
                        <input type="hidden" name="event_id" id="event_id" />
                        <div class="form-group">
                            <label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label">Title</label>
                            <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                                <input type="text" class="form-control" id="event_title" name="event_title">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label">Description</label>
                            <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                                <textarea class="form-control" style="height:55px;" id="event_desc" name="event_desc"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label">Event Start</label>
                            <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                                <div class="input-group date" id="startDatePicker">
                                    <input type="text" class="form-control" id="event_start_datetime_date" name="event_start_datetime_date" />
                                    <span class="input-group-addon">
                                       <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group date" id="startTimePicker">
                                    <input type="text" class="form-control" id="event_start_datetime_time" name="event_start_datetime_time" />
                                    <span class="input-group-addon">
                                       <span class="glyphicon glyphicon-time"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label">Event End</label>
                            <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                                <div class="input-group date" id="endDatePicker">
                                    <input type="text" class="form-control" id="event_end_datetime_date" name="event_end_datetime_date" />
                                    <span class="input-group-addon">
                                       <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <div class="input-group date" id="endTimePicker">
                                    <input type="text" class="form-control" id="event_end_datetime_time" name="event_end_datetime_time" />
                                    <span class="input-group-addon">
                                       <span class="glyphicon glyphicon-time"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label">Participants</label>
                            <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                                <select class="form-control" id="participantInput" onchange="updateParticipant()"></select>
                                <input type="hidden" id="participant_ids" name="participant_ids" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default antoclose" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary" name="submit" value="Save" form="antoform" />
<!--                <button type="submit" class="btn btn-primary">Save changes</button>-->
            </div>
        </div>
    </div>
</div>
<div id="CalenderModalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel2">View Entry</h4>
            </div>
            <div class="modal-body">

                <div id="testmodal2" style="padding: 5px 20px;">
                    <form id="antoform2" method="post" class="form-horizontal calender" role="form">
                        <input type="hidden" name="event_id_view" id="event_id_view" />
                        <div class="form-group">
                            <label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label">Title</label>
                            <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                                <input type="text" id="event_titleDiv" class="form-control" disabled />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label">Description</label>
                            <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                                <textarea id="event_descDiv" class="form-control" disabled></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label">Event Start</label>
                            <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                                <input type="text" id="startDatePickerDiv" class="form-control" disabled />
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <input type="text" id="startTimePickerDiv" class="form-control" disabled />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label">Event End</label>
                            <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                                <input type="text" id="endDatePickerDiv" class="form-control" disabled />
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <input type="text" id="endTimePickerDiv" class="form-control" disabled />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label">Participants</label>
                            <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                                <textarea class="form-control" id="participantsDiv" readonly style="width: 100%; min-width: 100%; max-width: 100%; height: 100px;"></textarea>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default antoclose2" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="fc_create" data-toggle="modal" data-target="#CalenderModalNew"></div>
<div id="fc_edit" data-toggle="modal" data-target="#CalenderModalEdit"></div>
<!-- /calendar modal -->

<script type="text/javascript">
    function updateParticipant(){
        var participant = $("#participantInput").val();
        $("#participant_ids").val(participant);
    }

    function loadParticipants(){
        if($('#event_id').val() > 0){
            var selected_event = $('#event_id').val();
        }else{
            var selected_event = $('#event_id_view').val();
        }
        $('#participantInput').val('');
        $('#participantInput').trigger('change');
        $.ajax({
            url: "calendar.php",
            type: "GET",
            data:  {
                ajaxTrigger: 1
                ,event_id: selected_event
            },
            dataType: 'json',
            success: function(data){
                <?=$_SESSION[SYSTEM_SESSION]['user_id'] > 0 ? "$('#participantInput').val(data[0]);" : "$('#participantsDiv').text(data);"?>
                <?=$_SESSION[SYSTEM_SESSION]['user_id'] > 0 ? "$('#participantInput').trigger('change');" : ""?>
            }
        });
    }
</script>