<form method="post" id="toSubmit"></form>
<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                <div class="panel-title">Edit Form</div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 col-sm-3 col-md-3 col-lg-3" >Mobile Number</label>
                <div class="col-sm-9 col-sm-9 col-md-9 col-lg-9">
                    <input type="number" class="form-control" form="toSubmit" name="dm_number" value="<?=$oData['dm_number']?>" required="" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 col-sm-3 col-md-3 col-lg-3" >Text</label>
                <div class="col-sm-9 col-sm-9 col-md-9 col-lg-9">
                    <textarea class="form-control" onkeyup="updateMes(this.value)" id="dm_content" name="dm_content" form="toSubmit" style="min-width: 100%; max-width: 100%; min-height: 70px; max-height: 70px;"><?=nl2br($oData['dm_content'])?></textarea>
                </div>
                <div class="col-sm-12 col-sm-12 col-md-12 col-lg-12"><p class="pull-right text-muted" id="autoGenMes">1 of 0/120</p></div>
            </div>
        </div>
        <div class="panel-footer">
            <a href="?inpage=<?=$_GET['inpage']?>" class="btn btn-danger">Cancel</a>
            <input type="submit" class="btn btn-success pull-right" value="Save" form="toSubmit" />
        </div>
    </div>
</div>
<script type="text/javascript">
    function updateMes(textTemp){
        var maxMessCount = 120;
        var textCount = textTemp.length;
        var curPage = 1;

        if(textCount >= maxMessCount){
            curPage = Math.floor((parseInt(textCount) / parseInt(maxMessCount)) + parseInt(1));
            textCount = textCount - ((parseInt(curPage) - parseInt(1)) * maxMessCount);
        }

        document.getElementById('autoGenMes').innerHTML = curPage + " of " + textCount + "/" + maxMessCount;
    }
</script>