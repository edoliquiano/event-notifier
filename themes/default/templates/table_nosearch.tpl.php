<?php
if (!empty($_SERVER['QUERY_STRING'])) {
    $qrystr = explode("&", $_SERVER['QUERY_STRING']);
    foreach ($qrystr as $value) {
        $qstr = explode("=", $value);
        if ($qstr[0] != "sortby" && $qstr[0] != "sortof") {
            $arrQryStr[] = implode("=", $qstr);
        }
        if ($qstr[0] != "search_field" && $qstr[0] != "p") {
            $qryFrm[] = "<input type='hidden' name='$qstr[0]' value='" . urldecode($qstr[1]) . "'>";
        }
    }
    $aQryStr = $arrQryStr;
    $aQryStr[] = "p=@@";
    $qryForms = (count($qryFrm) > 0) ? implode(" ", $qryFrm) : '';
}

$srchFormAction = $_SERVER['PHP_SELF'] . "?$queryStr";
?>

<!--<div class="row">-->
<!--    <div class="col-sm-8">-->
<!--        <div class="divTblContent">--><?php //echo $resultsInfo; ?><!--</div>-->
<!--    </div>-->
<!--    <div class="col-sm-4">-->
<!--        <form role="form" method="GET" action="">-->
<!--            <div class="form-group input-group pull-right">-->
<!--                <input type="text" class="form-control" placeholder="Search Keyword" name="search_field" value="--><?//= $_GET['search_field'] ?><!--">-->
<!--                <span class="input-group-btn">-->
<!--                    <button class="btn btn-default pull-right" type="button" ><i class="fa fa-search"></i></button>-->
<!--                </span>-->
<!--            </div>-->
<!--            --><?//= $qryForms ?>
<!--        </form>-->
<!--    </div>-->
<!--</div>-->

<form role="form" method="get" action="">
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col-md-12 center-block">
                            <input type="text" autofocus class="form-control input-lg" placeholder="SEARCH" name="search_field">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6 col-xs-6 center-block">
                            <button type="submit" class="btn btn-primary btn-lg" style="width: 100%">SEARCH</button>
                        </div>
                        <div class="col-md-6 col-xs-6 center-block">
                            <button type="button" class="btn btn-danger btn-lg" onclick="closeModal()" style="width: 100%">CANCEL</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?= $qryForms ?>
</form>

<div class="row">
    <div class="col-lg-12">

        <div class="table-responsive">
            <table class="table table-condensed table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <?php
                        foreach ($tblFields as $fkey => $fvalue) {
                            $aQryStr = $arrQryStr;
                            $aQryStr[] = "sortby=$fkey";
                            $aQryStr[] = "sortof=" . ((isset($_GET['sortof']) && $_GET['sortof'] == "asc") ? "desc" : "asc");
                            $queryStr = implode("&", $aQryStr);
                            $fvalue = (empty($fvalue)) ? "" : "<a href='?$queryStr'>$fvalue</a>";
                            $sortimg = isset($_GET['sortof']) ? (($_GET['sortof'] == "asc") ? "fa-sort-amount-asc" : "fa-sort-amount-desc") : "";
                            $sortimg = ($fkey == $_GET['sortby']) ? ' <i class="fa '.$sortimg.'"></i>' : "";
                            ?>
                            <th><?= $fvalue . $sortimg ?></th>
                            <?php
                        }
                        ?>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    if (count($tblData) > 0) {
                        foreach ($tblData as $dkey => $dvalue) {
                            ?>
                            <tr>
                                <?php
                                foreach ($tblFields as $fkey => $fvalue) {
                                    ?>
                                    <td  <?= (isset($attribs[$fkey]) ? $attribs[$fkey] : "") ?> ><?= $dvalue[$fkey] ?></td>
                                    <?php
                                }
                                ?>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr>
                            <td colspan="<?= count($tblFields) ?>" class="alert-warning" ><h5>No record found.</h5></td>
                        </tr>
                        <?php
                    }
                    ?>              
                </tbody>

            </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?= $resultsInfo ?>  
    </div>
</div>

<script language="JavaScript">
    function closeModal() {
        $(function () {
            $('#myModal').modal('toggle');
        });
    }
</script>

