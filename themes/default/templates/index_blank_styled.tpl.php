<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/png" href="<?=SYSTEM_ICON?>"/>

        <title><?= SYSCONFIG_TITLE ?></title>

<!--        <style type="text/css">-->
<!--            @import url('../site.css');-->
<!--        </style>-->
        <?php
            require_once('../site.php');
        ?>
    </head>

    <?php
        require_once("../scripts_top.php");
    ?>

    <body class="login">
        <?php
            if (is_object($centerPanel)){
                print $centerPanel->fetchBlock();
            }
        ?>
    </body>

    <?php
        require_once("../scripts_bottom.php");
    ?>
</html>
