<form method="post" enctype="multipart/form-data" id="toSubmit"></form>
<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                <div class="panel-title"><?=isset($_GET['edit']) ? "Edit" : "Add"?> Form</div>
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                <div class="pull-right">
                    <?php
                    if(count($oData) > 0){
                        if($oData['is_active'] == 1){
                            $status = 'checked';
                        }else{
                            $status = '';
                        }
                        ?>
                        <label>
                            <input type="checkbox" class="js-switch" <?=$status?> onclick="updateStatus()" />
                        </label>
                    <?php
                    }else{
                        ?>
                        <label>
                            <input type="checkbox" class="js-switch" onclick="updateStatus()" />
                        </label>
                    <?php
                    }
                    ?>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 col-sm-3 col-md-3 col-lg-3" >Name</label>
                <div class="col-sm-9 col-sm-9 col-md-9 col-lg-9">
                    <input type="text" class="form-control" form="toSubmit" name="si_name" value="<?=$oData['si_name']?>" required="" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 col-sm-3 col-md-3 col-lg-3" >Descripition</label>
                <div class="col-sm-9 col-sm-9 col-md-9 col-lg-9">
                    <textarea class="form-control" name="si_desc" form="toSubmit" style="min-width: 100%; max-width: 100%; min-height: 60px; max-height: 60px;"><?=$oData['si_desc']?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 col-sm-3 col-md-3 col-lg-3" >Image</label>
                <div class="col-sm-5 col-sm-5 col-md-5 col-lg-5">
                    <input type="file" class="form-control" name="si_img" form="toSubmit" />
                </div>
                <div class="col-sm-4 col-sm-4 col-md-4 col-lg-4">
                    <div class="img-responsive"><img src="?inpage=<?=$_GET['inpage']?>&image=<?=$_GET['edit']?>" style="width: 100%"></div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 col-sm-3 col-md-3 col-lg-3" >Order</label>
                <div class="col-sm-9 col-sm-9 col-md-9 col-lg-9">
                    <input type="number" class="form-control" form="toSubmit" name="si_ordinal" value="<?=$oData['si_ordinal']?>" required="" />
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <a href="?inpage=<?=$_GET['inpage']?>" class="btn btn-danger">Cancel</a>
            <input type="submit" class="btn btn-success pull-right" value="Save" form="toSubmit" />
        </div>
    </div>
</div>
<script type="text/javascript">
    function updateStatus(){
        $.ajax({
            url: "?inpage=<?=$_GET['inpage']?>&id=<?=$_GET['edit']?>",
            type: "GET",
            data:  {
                action : 'update_status'
            },
            success: function(data){
//                console.log(data);
            }
        });
    }
</script>