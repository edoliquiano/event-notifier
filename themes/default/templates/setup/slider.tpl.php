<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <a href="?inpage=<?=$_GET['inpage']?>&action=add" class="btn btn-dark">Add New</a>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="panel-title">Configuration List</div>
        </div>
        <div class="panel-body">
            <?php
                if(count($tblDataList) > 0){

                    foreach($tblDataList as $key=>$value){
                        ?>
                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                            <div class="thumbnail">
                                <div class="image view view-first">
                                    <img style="width: 100%; display: block;" src="?inpage=<?=$_GET['inpage']?>&image=<?=$value['si_id']?>" alt="image" />
                                    <div class="mask">
                                        <p><?=$value['si_name']?></p>
                                        <div class="tools tools-bottom">
                                            <!--                                <a href="#"><i class="fa fa-link"></i></a>-->
                                            <a href="?inpage=<?=$_GET['inpage']?>&edit=<?=$value['si_id']?>"><i class="fa fa-pencil"></i></a>
                                            <a href="?inpage=<?=$_GET['inpage']?>&delete=<?=$value['si_id']?>" onclick="return confirm('Are you sure, you want to delete?');"><i class="fa fa-times"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="caption">
                                    <p><?=nl2br($value['si_desc'])?></p>
                                </div>
                            </div>
                        </div>
                    <?php
                    }
                }else{
                    ?>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: center;">No Record Found</div>
                <?php
                }
            ?>
        </div>
    </div>
</div>