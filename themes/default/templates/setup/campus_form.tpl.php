<form method="post" id="toSubmit"></form>
<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                <div class="panel-title">Edit Form</div>
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                <div class="pull-right">
                    <?php
                        if(count($oData) > 0){
                            if($oData['is_active'] == 1){
                                $status = 'checked';
                            }else{
                                $status = '';
                            }
                        ?>
                            <label>
                                <input type="checkbox" class="js-switch" <?=$status?> onclick="updateStatus()" />
                            </label>
                    <?php
                        }else{
                        ?>
                            &nbsp;
                    <?php
                        }
                    ?>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 col-sm-3 col-md-3 col-lg-3" >Name</label>
                <div class="col-sm-9 col-sm-9 col-md-9 col-lg-9">
                    <input type="text" class="form-control" form="toSubmit" name="campus_name" value="<?=$oData['campus_name']?>" required="" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 col-sm-3 col-md-3 col-lg-3" >Short Name</label>
                <div class="col-sm-9 col-sm-9 col-md-9 col-lg-9">
                    <input type="text" class="form-control" form="toSubmit" name="campus_shortname" value="<?=$oData['campus_shortname']?>" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 col-sm-3 col-md-3 col-lg-3" >Description</label>
                <div class="col-sm-9 col-sm-9 col-md-9 col-lg-9">
                    <textarea class="form-control" style="width: 100%; max-width: 100%; min-width: 100%;" form="toSubmit" name="campus_desc" ><?=$oData['campus_desc']?></textarea>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <a href="?inpage=<?=$_GET['inpage']?>" class="btn btn-danger">Cancel</a>
            <input type="submit" class="btn btn-success pull-right" value="Save" form="toSubmit" />
        </div>
    </div>
</div>
<script type="text/javascript">
    function updateStatus(){
        $.ajax({
            url: "?inpage=<?=$_GET['inpage']?>&id=<?=$_GET['edit']?>",
            type: "GET",
            data:  {
                action : 'update_status'
            },
            success: function(data){
//                console.log(data);
            }
        });
    }
</script>