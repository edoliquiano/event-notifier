<!--<div>-->
<!--    <a class="hiddenanchor" id="signup"></a>-->
<!--    <a class="hiddenanchor" id="signin"></a>-->
<!---->
<!--    <div class="login_wrapper">-->
<!--        <div class="animate form login_form">-->
<!--            <section class="login_content">-->
<!--                <form class="form-horizontal" method="post" action="">-->
<!--                    <h1>Login Form</h1>-->
<!--                    <div>-->
<!--                        <input type="text" name="user_name" class="form-control" placeholder="Username" required="" />-->
<!--                    </div>-->
<!--                    <div>-->
<!--                        <input type="password" name="user_password" class="form-control" placeholder="Password" required="" />-->
<!--                    </div>-->
<!--                    <div>-->
<!--                        <input type="submit" class="btn btn-default" value="Log In" />-->
<!--                        <a class="btn btn-default submit" href="index.html">Log in</a>-->
<!--                        <a class="reset_pass" href="#">Lost your password?</a>-->
<!--                    </div>-->
<!---->
<!--                    <div class="clearfix"></div>-->
<!--                </form>-->
<!--            </section>-->
<!--        </div>-->
<!---->
<!--        <div id="register" class="animate form registration_form">-->
<!--            <section class="login_content">-->
<!--                <form>-->
<!--                    <h1>Create Account</h1>-->
<!--                    <div>-->
<!--                        <input type="text" class="form-control" placeholder="Username" required="" />-->
<!--                    </div>-->
<!--                    <div>-->
<!--                        <input type="email" class="form-control" placeholder="Email" required="" />-->
<!--                    </div>-->
<!--                    <div>-->
<!--                        <input type="password" class="form-control" placeholder="Password" required="" />-->
<!--                    </div>-->
<!--                    <div>-->
<!--                        <a class="btn btn-default submit" href="index.html">Submit</a>-->
<!--                    </div>-->
<!---->
<!--                    <div class="clearfix"></div>-->
<!---->
<!--                    <div class="separator">-->
<!--                        <p class="change_link">Already a member ?-->
<!--                            <a href="#signin" class="to_register"> Log in </a>-->
<!--                        </p>-->
<!---->
<!--                        <div class="clearfix"></div>-->
<!--                        <br />-->
<!---->
<!--                        <div>-->
<!--                            <h1><i class="fa fa-paw"></i> Gentelella Alela!</h1>-->
<!--                            <p>©2016 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </form>-->
<!--            </section>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->

<style>
    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 300;
        src: local('Open Sans Light'), local('OpenSans-Light'), url(https://fonts.gstatic.com/s/opensans/v15/mem5YaGs126MiZpBA-UN_r8OUuhs.ttf) format('truetype');
    }
    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 400;
        src: local('Open Sans Regular'), local('OpenSans-Regular'), url(https://fonts.gstatic.com/s/opensans/v15/mem8YaGs126MiZpBA-UFVZ0e.ttf) format('truetype');
    }
    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 600;
        src: local('Open Sans SemiBold'), local('OpenSans-SemiBold'), url(https://fonts.gstatic.com/s/opensans/v15/mem5YaGs126MiZpBA-UNirkOUuhs.ttf) format('truetype');
    }
    @font-face {
        font-family: 'Open Sans Condensed';
        font-style: normal;
        font-weight: 300;
        src: local('Open Sans Condensed Light'), local('OpenSansCondensed-Light'), url(https://fonts.gstatic.com/s/opensanscondensed/v12/z7NFdQDnbTkabZAIOl9il_O6KJj73e7Ff1GhDuXMQg.ttf) format('truetype');
    }
    @font-face {
        font-family: 'Open Sans Condensed';
        font-style: normal;
        font-weight: 700;
        src: local('Open Sans Condensed Bold'), local('OpenSansCondensed-Bold'), url(https://fonts.gstatic.com/s/opensanscondensed/v12/z7NFdQDnbTkabZAIOl9il_O6KJj73e7Ff0GmDuXMQg.ttf) format('truetype');
    }
    * {
        box-sizing: border-box;
    }
    body {
        font-family: 'open sans', helvetica, arial, sans;
        background: url(http://farm8.staticflickr.com/7064/6858179818_5d652f531c_h.jpg) no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
    }
    .log-form {
        width: 40%;
        min-width: 320px;
        max-width: 475px;
        background: #fff;
        position: absolute;
        top: 50%;
        left: 50%;
        -webkit-transform: translate(-50%, -50%);
        -moz-transform: translate(-50%, -50%);
        -o-transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%);
        box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.25);
    }
    @media (max-width: 40em) {
        .log-form {
            width: 95%;
            position: relative;
            margin: 2.5% auto 0 auto;
            left: 0%;
            -webkit-transform: translate(0%, 0%);
            -moz-transform: translate(0%, 0%);
            -o-transform: translate(0%, 0%);
            -ms-transform: translate(0%, 0%);
            transform: translate(0%, 0%);
        }
    }
    .log-form form {
        display: block;
        width: 100%;
        padding: 2em;
    }
    .log-form h2 {
        color: #ecf0f1;
        font-family: 'open sans condensed';
        font-size: 1.35em;
        display: block;
        background: #487eb0;
        width: 100%;
        text-transform: uppercase;
        padding: 0.75em 1em 0.75em 1.5em;
        box-shadow: inset 0px 1px 1px rgba(255, 255, 255, 0.05);
        border: 1px solid #487eb0;
        margin: 0;
        font-weight: bolder;
    }
    .log-form input {
        display: block;
        margin: auto auto;
        width: 100%;
        margin-bottom: 2em;
        padding: 0.5em 0;
        border: none;
        border-bottom: 1px solid #eaeaea;
        padding-bottom: 1.25em;
        color: #757575;
    }
    .log-form input:focus {
        outline: none;
    }
    .log-form .btn {
        display: inline-block;
        background: #487eb0;
        border: 1px solid #2980b9;
        padding: 0.5em 2em;
        color: white;
        margin-right: 0.5em;
        box-shadow: inset 0px 1px 0px rgba(255, 255, 255, 0.2);
    }
    .log-form .btn:hover {
        background: #23cad5;
    }
    .log-form .btn:active {
        background: #1fb5bf;
        box-shadow: inset 0px 1px 1px rgba(0, 0, 0, 0.1);
    }
    .log-form .btn:focus {
        outline: none;
    }
    .log-form .forgot {
        color: #2980b9;
        line-height: 0.5em;
        position: relative;
        top: 2.5em;
        text-decoration: none;
        font-size: 0.75em;
        margin: 0;
        padding: 0;
        float: right;
    }
    .log-form .forgot:hover {
        color: #1ba0a9;
    }

</style>

<div class="log-form">
    <img src="<?=SYSTEM_ICON?>" style="position: absolute; width: 10%; right: 0%;"><?=isset($errMsg) ? "<h2 style=\"color: #F97F51;\">".$errMsg."</h2>" : "<h2>Login to your account</h2>"?>
    <form method="post">
        <input type="text" title="username" placeholder="username" name="user_name" />
        <input type="password" title="username" placeholder="password" name="user_password" />
        <button type="submit" class="btn">Login</button>
<!--        <a class="forgot" href="#">Forgot Username?</a>-->
    </form>
</div><!--end log form -->