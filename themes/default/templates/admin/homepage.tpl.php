<div class="container">
    <div class="row">
        <div class="slider">
            <div class="img-responsive">
                <ul class="bxslider">
                    <?php
                        if(count($sliderList) > 0){
                            foreach($sliderList as $key=>$value){
                                ?>
                                <li><div style="background: url('setup.php?inpage=slider&image=<?=$value['si_id']?>') no-repeat center center; height: 100%; width: 100%;" /></li>
                            <?php
                            }
                        }else{
                            ?>
                            <li><div style="background: url('<?=SYSTEM_ICON?>') no-repeat center center; height: 100%; width: 100%;" /></li>
                        <?php
                        }
                    ?>
                </ul>
            </div>
        </div>
    </div>
</div>

<?php
    if(SYSTEM_DESC != '' || SYSTEM_DESC != null){
        ?>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="text-center">
                        <h2>Description</h2>
                        <p><?=SYSTEM_DESC?></p>
                    </div>
                    <hr>
                </div>
            </div>
        </div>
    <?php
    }
?>

<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="text-center">
                <h2 style="color: #487EB0;cursor: pointer;" title="View Calendar" onclick="window.location.href='calendar.php'">Click Here <i class="fa fa-calendar fa-1x"></i></h2>
                <p>&nbsp;</p>
            </div>
            <hr>
        </div>
    </div>
</div>

<div class="portfolio">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="text-center">
                    <h2>About Us</h2>
                    <!--                <p>Lorem ipsum dolor sit amet consectetur adipiscing elit Cras suscipit arcu<br> vestibulum volutpat libero sollicitudin vitae Curabitur ac aliquam <br> lorem sit amet scelerisque justo</p>-->
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 wow bounceIn" data-wow-offset="0" data-wow-delay="0.4s">
                <a href="../components/enno/img/26.jpg" class="flipLightBox">
                    <img src="../components/enno/img/26.jpg" width="auto" height="230" alt="Image 1" />
                    <span><b>LightBox Group 1, Image 1</b> Text to accompany first lightbox image</span>
                </a>
            </div>
            <div class="col-md-3 wow bounceIn" data-wow-offset="0" data-wow-delay="1.0s">
                <a href="../components/enno/img/27.jpg" class="flipLightBox">
                    <img src="../components/enno/img/27.jpg" width="auto" height="230" alt="Image 2" />
                    <span><b>LightBox Group 1, Image 2</b><br />Text to accompany 2nd lightbox image</span>
                </a>
            </div>
            <div class="col-md-3 wow bounceIn" data-wow-offset="0" data-wow-delay="1.4s">
                <a href="../components/enno/img/28.jpg" class="flipLightBox">
                    <img src="../components/enno/img/28.jpg" width="auto" height="230" alt="Image 3" />
                    <span><b>LightBox Group 1, Three</b> Text to accompany the third lightbox image</span>
                </a>
            </div>
            <div class="col-md-3 wow bounceIn" data-wow-offset="0" data-wow-delay="2.0s">
                <a href="../components/enno/img/29.jpg" class="flipLightBox">
                    <img src="../components/enno/img/29.jpg" width="auto" height="230" alt="Image 4" />
                    <span><b>The Group 1 Final LightBox</b> Text to accompany the last of the lighboxes</span>
                </a>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="box">
            <div class="col-md-12">
                &nbsp;
            </div>
        </div>
    </div>
</div>