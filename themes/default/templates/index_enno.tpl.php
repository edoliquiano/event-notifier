<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="<?=SYSTEM_ICON?>"/>

    <title><?= SYSCONFIG_TITLE ?></title>

    <!-- Bootstrap -->
    <link href="../components/enno/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../components/enno/css/animate.css">
    <link rel="stylesheet" href="../components/enno/css/font-awesome.min.css">
    <link rel="stylesheet" href="../components/enno/css/jquery.bxslider.css">
    <link rel="stylesheet" type="text/css" href="../components/enno/css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="../components/enno/css/demo.css" />
    <link rel="stylesheet" type="text/css" href="../components/enno/css/set1.css" />
    <link href="../components/enno/css/overwrite.css" rel="stylesheet">
    <link href="../components/enno/css/style.css" rel="stylesheet">
</head>

<body>
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <h1 style="font-size: 50px;">Welcome! <?=SYSCONFIG_TITLE?></h1>
        </div>
        <div class="navbar-collapse collapse">
            <div class="menu">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation"><a href="?login"><img src="<?=SYSTEM_ICON?>" style="width: 100px;" /></a></li>
                </ul>
            </div>
        </div>
    </div>
</nav>

</br></br></br>

<?php
if (is_object($centerPanel))
    print $centerPanel->fetchBlock();
?>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="../components/enno/js/jquery-2.1.1.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../components/enno/js/bootstrap.min.js"></script>
<script src="../components/enno/js/wow.min.js"></script>
<script src="../components/enno/js/jquery.easing.1.3.js"></script>
<script src="../components/enno/js/jquery.isotope.min.js"></script>
<script src="../components/enno/js/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="../components/enno/js/fliplightbox.min.js"></script>
<script src="../components/enno/js/functions.js"></script>
<script type="text/javascript">
    $('.portfolio').flipLightBox()
</script>

</body>

</html>
